import Cookies from "universal-cookie";

export const fetchWrapper = {
  get,
  post,
  put,
  delete: _delete,
  postFormData,
  putFormData
};

function get(url) {
  const requestOptions = {
    method: "GET",
    headers: { ...authHeader() },
  };
  return fetch(url, requestOptions).then(handleResponse);
}

function post(url, body, csrf) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json", ...authHeader() },
    credentials: "include",
    body: JSON.stringify(body),
    mode: "cors",
  };
  return fetch(url, requestOptions).then(handleResponse);
}

function postFormData(url, body, csrf) {
  const requestOptions = {
    method: "POST",
    headers: { ...authHeader() },
    credentials: "include",
    body: body,
    mode: "cors",
  };
  return fetch(url, requestOptions).then(handleResponse);
}

function putFormData(url, body, csrf) {
  const requestOptions = {
    method: "PUT",
    headers: { ...authHeader() },
    credentials: "include",
    body: body,
    mode: "cors",
  };
  return fetch(url, requestOptions).then(handleResponse);
}

function put(url, body) {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json", ...authHeader() },
    body: JSON.stringify(body),
  };
  return fetch(url, requestOptions).then(handleResponse);
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(url) {
  const requestOptions = {
    method: "DELETE",
    headers: { "Content-Type": "application/json", ...authHeader() },
    // headers: authHeader(url),
  };
  return fetch(url, requestOptions).then(handleResponse);
}

// helper functions

function authHeader() {
  const cookies = new Cookies();
  const cookie = cookies.get("Authorization");

  if (cookie) {
    return { Authorization: `Bearer ${cookie}` };
  } else {
    return {};
  }
}

function csrfHeader(csrf) {
  // return csrf
  return { "csrf-token": csrf };
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);

    return data;
  });
}
