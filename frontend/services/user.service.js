import getConfig from "next/config";
import Router from "next/router";
import cookieCutter from "cookie-cutter";

import { fetchWrapper } from "../helpers/fetch-wrapper";
import router from "next/router";

const { publicRuntimeConfig } = getConfig();
const baseUrl = `${publicRuntimeConfig.apiUrl}`;

export const userService = {
  login,
  register,
  logout,
  getAll,
  getById,
  updateById,
  deleteById,
  createUser,
  getMe,
  getTeachers,
  getDevice,
  getAvailableTeachers,
  changeDeviceMode,
  getAllNewRfid
};

function login(username, password) {
  return fetchWrapper
    .post(`${baseUrl}/login`, { username, password })
    .then((user) => {
      cookieCutter.set("Authorization", user.token);
      return user;
    })
    .catch((err) => {
      //   cookieCutter.set('Authorization', '', { expires: new Date(0) })
      return err;
    });
}

function getMe() {
  return fetchWrapper
    .get(`${baseUrl}/users/me`)
    .then((user) => {
      return user;
    })
    .catch((err) => {
      //   cookieCutter.set('Authorization', '', { expires: new Date(0) })
      return err;
    });
}

function register(values) {
  return fetchWrapper
    .post(`${baseUrl}/register`, { ...values })
    .then((user) => {
      return user;
    })
    .catch((err) => {
      console.log(err);
    });
}

function createUser(values) {
  for (var pair of values.entries()) {
    console.log(pair[0] + ", " + pair[1]);
  }
  return fetchWrapper
    .postFormData(`${baseUrl}/users`, values)
    .then((user) => {
      return user;
    })
    .catch((err) => {
      console.log(err);
    });
}

function logout() {
  // remove user from local storage, publish null to user subscribers and redirect to login page
  return fetchWrapper
    .post(`${baseUrl}/logout`)
    .then((user) => {
      cookieCutter.set("Authorization", "", { expires: new Date(0) });
      Router.push("/");
    })
    .catch((err) => {
      //   cookieCutter.set('Authorization', '', { expires: new Date(0) })
      console.log(err);
    });
}

function getAll() {
  const data = fetchWrapper.get(`${baseUrl}/users`);

  return data;
}

function getTeachers() {
  const data = fetchWrapper.get(`${baseUrl}/users/teachers`);

  return data;
}

function getAvailableTeachers() {
  const data = fetchWrapper.get(`${baseUrl}/users/available-teachers`);

  return data;
}

function getById(id) {
  const data = fetchWrapper.get(`${baseUrl}/users/${id}`);

  return data;
}

function updateById(id, values) {
  const data = fetchWrapper.putFormData(`${baseUrl}/users/${id}`, values);

  return data;
}

function deleteById(id) {
  const data = fetchWrapper.delete(`${baseUrl}/users/${id}`);

  return data;
}

function getDevice() {
  const data = fetchWrapper.get(`${baseUrl}/users/device`);

  return data;
}

function changeDeviceMode(values) {
  const data = fetchWrapper.put(`${baseUrl}/users/device`, values);

  return data;
}

function getAllNewRfid() {
  const data = fetchWrapper.get(`${baseUrl}/users/new-rfid`);

  return data;
}