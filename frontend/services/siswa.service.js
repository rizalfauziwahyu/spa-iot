import getConfig from "next/config";

import { fetchWrapper } from "../helpers/fetch-wrapper";

const { publicRuntimeConfig } = getConfig();
const baseUrl = `${publicRuntimeConfig.apiUrl}`;

export const siswaService = {
  index,
  show,
  store,
  update,
  destroy
};

async function index() {
  try {
    const users = await fetchWrapper.get(`${baseUrl}/users`);

    return users;
  } catch (err) {
    console.log(err);
  }
}

async function show(id) {
  try {
    const users = await fetchWrapper.get(`${baseUrl}/users/${id}`);

    return users;
  } catch (err) {
    console.log(err);
  }
}

async function store(userData) {
  try {
    const response = await fetchWrapper.post(`${baseUrl}/users`, userData);

    return response;
  } catch (err) {
    console.log(err);
  }
}

async function update(userId, userData) {
  try {
    const response = await fetchWrapper.putFormData(
      `${baseUrl}/users/${userId}`,
      userData
    );

    return response;
  } catch (err) {
    console.log(err);
  }
}

async function destroy(userId) {
  try {
    const response = await fetchWrapper.delete(
      `${baseUrl}/users/${userId}`, 
    );

    return response;
  } catch (err) {
    console.log(err);
  }
}
