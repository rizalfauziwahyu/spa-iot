import getConfig from "next/config";

import { fetchWrapper } from "../helpers/fetch-wrapper";

const { publicRuntimeConfig } = getConfig();
const baseUrl = `${publicRuntimeConfig.apiUrl}`;

export const pickupService = {
  index,
};

async function index() {
  try {
    const response = await fetchWrapper.get(`${baseUrl}/pickups`);

    return response;
  } catch (err) {
    console.log(err);
  }
}