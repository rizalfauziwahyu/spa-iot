import getConfig from "next/config";

import { fetchWrapper } from "../helpers/fetch-wrapper";

const { publicRuntimeConfig } = getConfig();
const baseUrl = `${publicRuntimeConfig.apiUrl}`;

export const kelasService = {
  index,
  show,
  store,
  updateById,
  destroy
};

async function index() {
  try {
    const classrooms = await fetchWrapper.get(`${baseUrl}/classrooms`);

    return classrooms;
  } catch (err) {
    console.log(err);
  }
}

async function show(id) {
  try {
    const classrooms = await fetchWrapper.get(`${baseUrl}/classrooms/${id}`);

    return classrooms;
  } catch (err) {
    console.log(err);
  }
}

async function updateById(id, values) {
  try {
    const classrooms = await fetchWrapper.put(`${baseUrl}/classrooms/${id}`, values);

    return classrooms;
  } catch (err) {
    console.log(err);
  }
}


async function store(classroomData) {
  try {
    const task = await fetchWrapper.post(`${baseUrl}/classrooms`, {
      ...classroomData
    });

    return task;
  } catch (err) {
    console.log(err);
  }
}

async function destroy(id) {
  try {
    const response = await fetchWrapper.delete(
      `${baseUrl}/classrooms/${id}`, 
    );

    return response;
  } catch (err) {
    console.log(err);
  }
}
