import getConfig from "next/config";

import { fetchWrapper } from "../helpers/fetch-wrapper";

const { publicRuntimeConfig } = getConfig();
const baseUrl = `${publicRuntimeConfig.apiUrl}`;

export const taskService = {
  index,
  show,
  store,
  upload3dFile,
  checkStatusRender,
  submitJob,
  archieveRendredImages,
  downloadArchieve
};

async function index() {
  try {
    const tasks = await fetchWrapper.get(`${baseUrl}/tasks`);

    return tasks;
  } catch (err) {
    console.log(err);
  }
}

async function show(id) {
  try {
    const tasks = await fetchWrapper.get(`${baseUrl}/tasks/${id}`);

    return tasks;
  } catch (err) {
    console.log(err);
  }
}

async function store(title, frameStart, frameEnd, csrf) {
  try {
    const task = await fetchWrapper.post(
      `${baseUrl}/tasks`,
      {
        title,
        frameStart,
        frameEnd,
      },
      csrf
    );

    return task;
  } catch (err) {
    console.log(err);
  }
}

async function upload3dFile(formData, taskId) {
  try {
    const task = await fetchWrapper.upload(
      `${baseUrl}/file3d/upload/${taskId}`,
      formData
    );
    return task;
  } catch (err) {
    console.log(err);
  }
}

async function checkStatusRender(id) {
  try {
    const response = await fetchWrapper.get(`${baseUrl}/file3d/check/${id}`);

    return response;
  } catch (err) {
    console.log(err);
  }
}

async function archieveRendredImages(id) {
  try {
    const response = await fetchWrapper.get(`${baseUrl}/file3d/createArchieve/${id}`);

    return response;
  } catch (err) {
    console.log(err);
  }
}

async function downloadArchieve(id) {
  try {
    const response = await fetchWrapper.download(`${baseUrl}/file3d/download/${id}`);
  
    return response.blob();
  } catch (err) {
    console.log(err);
  }
}

async function submitJob(id) {
  try {
    const task = await fetchWrapper.post(`${baseUrl}/render/submitjob`, {
      taskId: id,
    });
    return task;
  } catch (err) {
    console.log(err);
  }
}
