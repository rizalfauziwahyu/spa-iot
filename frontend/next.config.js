const publicRuntimeConfig = {
  apiUrl:
    process.env.NODE_ENV === 'development'
      ? 'http://localhost:3000/api/v1' // development api
      : 'https://sichjombang.com/api/v1', // production api
};

module.exports = {
  publicRuntimeConfig,
};
