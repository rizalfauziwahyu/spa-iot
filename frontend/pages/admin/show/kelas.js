import Admin from 'layouts/Admin';
import DetailKelas from 'components/Cards/DetailKelas';

function ShowKelas() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <DetailKelas />
        </div>
      </div>
    </Admin>
  );
}

export default ShowKelas;