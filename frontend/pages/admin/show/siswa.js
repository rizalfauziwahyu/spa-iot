import Admin from 'layouts/Admin';
import DetailSiswa from 'components/Cards/DetailSiswa';

function ShowSiswa() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <DetailSiswa />
        </div>
      </div>
    </Admin>
  );
}

export default ShowSiswa;
