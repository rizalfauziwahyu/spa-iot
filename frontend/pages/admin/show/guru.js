import Admin from 'layouts/Admin';
import DetailSiswa from 'components/Cards/DetailSiswa';
import DetailGuru from 'components/Cards/DetailGuru';

function ShowGuru() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <DetailGuru />
        </div>
      </div>
    </Admin>
  );
}

export default ShowGuru;