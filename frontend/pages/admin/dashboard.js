import React from 'react';

// components

import DataTable from 'components/Cards/DataTable';
import SiswaTable from 'components/Cards/SiswaTable';
import GuruTable from 'components/Cards/GuruTable';

// layout for page

import Admin from 'layouts/Admin.js';

export default function Dashboard() {
  return (
    <Admin>
      <div className="flex flex-wrap">
        <div className="w-full xl:w-8/12 mb-12 xl:mb-0 px-4">{/* <CardLineChart /> */}</div>
        <div className="w-full xl:w-4/12 px-4">{/* <CardBarChart /> */}</div>
      </div>
      <div className="flex flex-wrap mt-4">
        <div className="w-full xl:w-8/12 mb-12 xl:mb-0 px-4">
          <SiswaTable heading="Daftar Siswa" />
        </div>
        <div className="w-full xl:w-4/12 px-4">
          <DataTable heading="Daftar Kelas" />
        </div>
      </div>
      <div className="flex flex-wrap mt-4">
        <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-4">
          <GuruTable heading="Daftar Wali Kelas" />
        </div>
      </div>
    </Admin>
  );
}
