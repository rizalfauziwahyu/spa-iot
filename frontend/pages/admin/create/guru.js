import Admin from 'layouts/Admin';

import CreateFormGuru from 'components/Forms/Guru/CreateFormGuru';

function CreateGuru() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
            <CreateFormGuru />
        </div>
      </div>
    </Admin>
  );
}

export default CreateGuru;
