import Admin from 'layouts/Admin';
import CreateFormSiswa from 'components/Forms/Siswa/CreateFormSiswa';

function CreateSiswa() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <CreateFormSiswa />
        </div>
      </div>
    </Admin>
  );
}

export default CreateSiswa;
