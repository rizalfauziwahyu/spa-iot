import Admin from 'layouts/Admin';
import CreateFormKelas from 'components/Forms/Kelas/CreateFormKelas';

function CreateKelas() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <CreateFormKelas />
        </div>
      </div>
    </Admin>
  );
}

export default CreateKelas;
