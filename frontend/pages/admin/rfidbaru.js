import React from 'react';

// components

import RfidTable from 'components/Cards/RfidTable';

// layout for page

import Admin from 'layouts/Admin.js';

export default function Rfidbaru() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <RfidTable  heading="Daftar RFID Baru"/>
        </div>
      </div>
    </Admin>
  );
}
