import React, { useState, useEffect } from 'react';

// components

import CardTable from 'components/Cards/CardTable.js';
import GuruTable from 'components/Cards/GuruTable';

// layout for page

import Admin from 'layouts/Admin.js';

export default function Siswa() {

  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <GuruTable heading="Daftar Wali Kelas" />
        </div>
      </div>
    </Admin>
  );
}
