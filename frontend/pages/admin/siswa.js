import React, { useState, useEffect } from "react";
import { userService } from "services/user.service";
// components
import Admin from "layouts/Admin.js";
import TableStudents from "components/Table";
import TableDropdown from "components/Dropdowns/TableDropdown";

const columns = [
  {
    label: "Nama",
    accessor: "name",
    sortable: true,
  },
  {
    label: "Kelas",
    accessor: "classroom",
    sortable: true,
  },
  {
    label: "Username",
    accessor: "username",
    sortable: true,
  },
  {
    label: "No HP",
    accessor: "phone",
    sortable: false,
  },
  {
    label: "Jenis Kelamin",
    accessor: "gender",
    sortable: true,
  },
  {
    label: "Aksi",
    accessor: "action",
    sortable: false,
  }
];
// layout for page

export default function Siswa() {
  const [data, setData] = useState([]);

  useEffect(() => {
    userService.getAll().then((res) => {
      const results = res.data.map((item) => {
        const gender = item.gender === "FEMALE" ? "PEREMPUAN" : "LAKI-LAKI";
        return Object.assign(
          {},
          {
            name: item.name,
            classroom: item.classroom.title,
            username: item.username,
            phone: item.phone,
            gender: gender,
            action: <TableDropdown
            showHref={`/admin/show/siswa?id=${item.id}`}
            editHref={`/admin/edit/siswa?id=${item.id}`}
            deleteHref="users"
            id={item.id}
          />
          }
        );
      });
      setData(results);
    });
  }, [setData]);

  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <TableStudents caption="Daftar siswa" data={data} columns={columns} />
        </div>
      </div>
    </Admin>
  );
}
