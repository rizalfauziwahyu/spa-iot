import Admin from 'layouts/Admin';
import EditFormKelas from 'components/Forms/Kelas/EditFormKelas';

function EditKelas() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <EditFormKelas />
        </div>
      </div>
    </Admin>
  );
}

export default EditKelas;
