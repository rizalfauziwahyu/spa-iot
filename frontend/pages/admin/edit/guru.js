import Admin from 'layouts/Admin';

import EditFormGuru from 'components/Forms/Guru/EditFormGuru';

function EditGuru() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
            <EditFormGuru />
        </div>
      </div>
    </Admin>
  );
}

export default EditGuru;
