import Admin from 'layouts/Admin';
import EditFormSiswa from 'components/Forms/Siswa/EditFormSiswa';

function EditSiswa() {
  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <EditFormSiswa />
        </div>
      </div>
    </Admin>
  );
}

export default EditSiswa;
