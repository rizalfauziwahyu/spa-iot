import React from 'react';

// components

import AlatTable from 'components/Cards/AlatTable';


// layout for page

import Admin from 'layouts/Admin.js';

export default function Kelas() {


  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <AlatTable  heading="Perangkat RFID"/>
        </div>
      </div>
    </Admin>
  );
}

