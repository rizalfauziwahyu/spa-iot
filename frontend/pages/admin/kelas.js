import React, { useState, useEffect } from "react";

// layout for page
import Admin from "layouts/Admin.js";

// components
import TableClassroom from "components/Table";
import TableDropdown from "components/Dropdowns/TableDropdown";

import { kelasService } from "services/kelas.service";

const columns = [
  {
    label: "Kelas",
    accessor: "classroom",
    sortable: true,
  },
  {
    label: "Walikelas",
    accessor: "homeroom",
    sortable: true,
  },
  {
    label: "Kode Suara",
    accessor: "soundNumber",
    sortable: false,
  },
  {
    label: "Jumlah Siswa",
    accessor: "students",
    sortable: false,
  },
  {
    label: "Aksi",
    accessor: "action",
    sortable: false,
  },
];

export default function Kelas() {
  const [data, setData] = useState([]);

  useEffect(() => {
    (async () => {
      const response = await kelasService.index();
      const result = response.data.map((item) => {
        return Object.assign(
          {},
          {
            classroom: item.title,
            homeroom: item.teacher.name,
            soundNumber: item.soundNumber,
            students: item.User.length,
            action: (
              <TableDropdown
                showHref={`/admin/show/kelas?id=${item.id}`}
                editHref={`/admin/edit/kelas?id=${item.id}`}
                deleteHref="classrooms"
                id={item.id}
              />
            ),
          }
        );
      });
      setData(result);
    })();
  }, [setData]);



  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <TableClassroom
            caption="Daftar Kelas"
            data={data}
            columns={columns}
          />
        </div>
      </div>
    </Admin>
  );
}
