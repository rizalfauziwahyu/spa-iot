import React, { useEffect, useState } from "react";
import moment from "moment";
import TablePickups from "components/Table";
import { pickupService } from "services/pickup.service";
// layout for page

import Admin from "layouts/Admin.js";

const columns = [
  {
    label: "Siswa",
    accessor: "name",
    sortable: true,
  },
  {
    label: "Kelas",
    accessor: "classroom",
    sortable: true,
  },
  {
    label: "Status",
    accessor: "status",
    sortable: false,
  },
  {
    label: "Tanggal",
    accessor: "date",
    sortable: true,
  },
  {
    label: "Waktu",
    accessor: "datetime",
    sortable: false,
  },
];

export default function Penjemputan() {
  const [data, setData] = useState([]);

  useEffect(() => {
    (async () => {
      const result = await pickupService.index();
      const finalResult = result.data.map((item) => {
        const status =
          item.status === "PICKUP" ? "SUDAH DIJEMPUT" : "BELUM DIJEMPUT";
        const date = moment(item.date).format("DD/MM/YYYY");
        return Object.assign(
          {},
          {
            id: item.id,
            name: item.user.name,
            classroom: item.user.classroom.title,
            status: status,
            date: date,
            datetime: new Date(item.date).toLocaleTimeString(),
          }
        );
      });
      setData(finalResult);
    })();
  }, [setData]);

  return (
    <Admin>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <TablePickups
            caption="Log Penjemputan"
            data={data}
            columns={columns}
          />
        </div>
      </div>
    </Admin>
  );
}
