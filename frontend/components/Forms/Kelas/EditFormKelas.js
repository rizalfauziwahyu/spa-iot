import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { userService } from "services/user.service";
import { kelasService } from "services/kelas.service";

function EditFormKelas() {
  const router = useRouter();
  const { register, handleSubmit } = useForm();
  const [classroom, setClassroom] = useState({});
  const [teachers, setTeachers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const { id } = router.query;

  useEffect(() => {
    userService.getAvailableTeachers().then((res) => {
      setTeachers(res.data);
    });
  }, [setTeachers]);

  useEffect(() => {
    if (!id) return;
    kelasService.show(id).then((res) => {
      setClassroom(res.data);
      setIsLoading(false);
    });
  }, [id, setClassroom]);

  const onSubmit = async (data) => {
    data.teacherId = Number(data.teacherId);
    data.soundNumber = Number(data.soundNumber);
    const res = await kelasService.updateById(id, data);
    if (res.message === "updated") {
      router.push(`/admin/show/kelas/?id=${id}`);
    }
  };

  return (
    <>
      {isLoading && id ? (
        <button type="button" class="bg-indigo-500 ..." disabled>
          <svg class="animate-spin h-5 w-5 mr-3 ..." viewBox="0 0 24 24"></svg>
          Processing...
        </button>
      ) : (
        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg mt-16">
          <div className="px-6">
            <div className="rounded-t mb-0 px-4 py-3 border-0">
              <div className="flex flex-wrap items-center">
                <div className="relative w-full max-w-full flex-grow flex-1">
                  <h3 className="font-semibold text-lg text-blueGray-700">
                    Edit Data Kelas
                  </h3>
                </div>
              </div>
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div class="overflow-hidden sm:rounded-md">
                <div class="px-4 py-5 sm:p-6">
                  <div class="grid grid-cols-6 gap-6">
                    <div class="col-span-6 sm:col-span-3">
                      <label
                        for="title"
                        class="block text-sm font-medium text-gray-700"
                      >
                        Nama Kelas
                      </label>
                      <input
                        type="text"
                        name="title"
                        id="title"
                        autocomplete="given-name"
                        defaultValue={classroom.title}
                        required
                        {...register("title", { required: true })}
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>

                    <div class="col-span-6 sm:col-span-3">
                      <label
                        for="soundNumber"
                        class="block text-sm font-medium text-gray-700"
                      >
                        Kode Suara
                      </label>
                      <input
                        type="number"
                        name="soundNumber"
                        id="soundNumber"
                        autocomplete="given-name"
                        defaultValue={classroom.soundNumber}
                        required
                        {...register("soundNumber", { required: true })}
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>

                    <div class="col-span-6 sm:col-span-3">
                      <label
                        for="teacherId"
                        class="block text-sm font-medium text-gray-700"
                      >
                        Wali Kelas
                      </label>
                      <select
                        id="teacherId"
                        name="teacherId"
                        autocomplete="teacherId"
                        {...register("teacherId", { required: true })}
                        required
                        class="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                      >
                        {classroom.teacherId ? (
                          <option value={classroom.teacherId}>
                            {classroom.teacher.name}
                          </option>
                        ) : (
                          ""
                        )}

                        <option value="">Pilih Wali Kelas</option>
                        {teachers.map((teacher, index) => {
                          return (
                            <option value={teacher.id}>{teacher.name}</option>
                          );
                        })}
                      </select>
                    </div>
                  </div>
                </div>
                <div class="bg-blue-50 px-4 py-3 text-right sm:px-6">
                  <button
                    type="submit"
                    class="inline-flex justify-center border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline"
                  >
                    Simpan
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  );
}

export default EditFormKelas;
