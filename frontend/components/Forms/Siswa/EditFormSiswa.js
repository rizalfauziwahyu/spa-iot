import React, { useEffect, useState } from "react";
import { userService } from "services/user.service";
import { kelasService } from "services/kelas.service";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";

function EditFormSiswa() {
  const router = useRouter();
  const { register, handleSubmit } = useForm();
  const [user, setUser] = useState({});
  const [classrooms, setClassrooms] = useState([]);

  const { id } = router.query;

  useEffect(() => {
    kelasService.index().then((response) => {
      setClassrooms(response.data);
    });
  }, [setClassrooms]);

  useEffect(() => {
    if (!id) return;
    userService.getById(id).then((response) => {
      setUser(response.data);
    });
  }, [id, setUser]);

  const onSubmit = async (data) => {
    const formData = new FormData();
    if (data.file) {
      formData.append("file", data.file[0]);
    }

    formData.append("username", data.username);
    formData.append("password", data.password);
    formData.append("name", data.name);
    formData.append("classroomId", data.classroomId);
    formData.append("gender", data.gender);
    formData.append("rfid", data.rfid);
    formData.append("address", data.address);
    formData.append("phone", data.phone);
    formData.append("soundNumber", data.soundNumber);
    formData.append("parent", data.parent);
    formData.append("parentPhone", data.parentPhone);

    for (var pair of formData.entries()) {
      console.log(pair[0] + ", " + pair[1]);
    }

    const res = await userService.updateById(id, formData);
    if (res.message === "updated") {
      router.push(`/admin/show/siswa/?id=${id}`);
    }
  };

  return (
    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg mt-16">
      <div className="px-6">
        <div className="rounded-t mb-0 px-4 py-3 border-0">
          <div className="flex flex-wrap items-center">
            <div className="relative w-full max-w-full flex-grow flex-1">
              <h3 className="font-semibold text-lg text-blueGray-700">
                Edit Data Siswa
              </h3>
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div class="overflow-hidden sm:rounded-md">
            <div class="px-4 py-5 sm:p-6">
              <div class="grid grid-cols-6 gap-6">
                <div class="col-span-6 sm:col-span-3">
                  <label
                    for="username"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Nomor Kartu RFID
                  </label>
                  <input
                    type="text"
                    name="rfid"
                    id="rfid"
                    autocomplete="given-name"
                    defaultValue={user.rfid}
                    {...register("rfid", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>
                <div class="col-span-6 sm:col-span-3">
                  <label
                    for="username"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Username
                  </label>
                  <input
                    type="text"
                    name="username"
                    id="username"
                    autocomplete="given-name"
                    defaultValue={user.username}
                    {...register("username", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-3">
                  <label
                    for="password"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    autocomplete="family-name"
                    {...register("password")}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-4">
                  <label
                    for="name"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Nama Lengkap
                  </label>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    autoComplete="name"
                    defaultValue={user.name}
                    {...register("name", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-4">
                  <label
                    for="file"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Upload Foto Profil
                  </label>
                  <input
                    type="file"
                    name="file"
                    id="file"
                    autocomplete="file"
                    {...register("file")}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-3">
                  <label
                    for="classroomId"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Kelas
                  </label>
                  <select
                    id="classroomId"
                    name="classroomId"
                    autocomplete="classroomId"
                    {...register("classroomId", { required: true })}
                    class="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  >
                    <option value="">Pilih Kelas</option>
                    {classrooms.map((classroom, index) => {
                      return classroom.id === user.classroomId ? (
                        <option value={classroom.id} key={index} selected>
                          {classroom.title}
                        </option>
                      ) : (
                        <option value={classroom.id}>{classroom.title}</option>
                      );
                    })}
                  </select>
                </div>

                <div class="col-span-6 sm:col-span-3">
                  <label
                    for="gender"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Jenis Kelamin
                  </label>
                  <select
                    id="gender"
                    name="gender"
                    autocomplete="gender"
                    {...register("gender", { required: true })}
                    class="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  >
                    {user.gender === "MALE" ? (
                      <>
                        <option value="MALE" selected>
                          Laki-laki
                        </option>
                        <option value="FEMALE">Perempuan</option>
                      </>
                    ) : (
                      <>
                        <option value="MALE">Laki-laki</option>
                        <option value="FEMALE" selected>
                          Perempuan
                        </option>
                      </>
                    )}
                  </select>
                </div>

                <div class="col-span-6">
                  <label
                    for="address"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Alamat
                  </label>
                  <input
                    type="text"
                    name="address"
                    id="address"
                    autocomplete="address"
                    defaultValue={user.address}
                    {...register("address", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                  <label
                    for="phone"
                    class="block text-sm font-medium text-gray-700"
                  >
                    No HP
                  </label>
                  <input
                    type="text"
                    name="phone"
                    id="phone"
                    autoComplete="phone"
                    defaultValue={user.phone}
                    {...register("phone", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-4">
                  <label
                    for="name"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Nomor Suara Sistem
                  </label>
                  <input
                    type="text"
                    name="soundNumber"
                    id="soundNumber"
                    {...register("soundNumber", { required: true })}
                    defaultValue={user.soundNumber}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                {user.parent ? (
                  <>
                    <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                      <label
                        for="parent"
                        class="block text-sm font-medium text-gray-700"
                      >
                        Nama Orang Tua / Wali
                      </label>
                      <input
                        type="text"
                        name="parent"
                        id="parent"
                        {...register("parent", { required: true })}
                        defaultValue={user.parent.name}
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>
                    <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                      <label
                        for="parentPhone"
                        class="block text-sm font-medium text-gray-700"
                      >
                        No HP Orang Tua / Wali
                      </label>
                      <input
                        type="text"
                        name="parentPhone"
                        id="parentPhone"
                        autocomplete="parentPhone"
                        defaultValue={user.parent.phone}
                        {...register("parentPhone", { required: true })}
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>
                  </>
                ) : (
                  <>
                    <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                      <label
                        for="parent"
                        class="block text-sm font-medium text-gray-700"
                      >
                        Nama Orang Tua / Wali
                      </label>
                      <input
                        type="text"
                        name="parent"
                        id="parent"
                        {...register("parent")}
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>

                    <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                      <label
                        for="parentPhone"
                        class="block text-sm font-medium text-gray-700"
                      >
                        No HP Orang Tua / Wali
                      </label>
                      <input
                        type="text"
                        name="parentPhone"
                        id="parentPhone"
                        autocomplete="parentPhone"
                        {...register("parentPhone")}
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                      />
                    </div>
                  </>
                )}
              </div>
            </div>
            <div class="bg-blue-50 px-4 py-3 text-right sm:px-6">
              <button
                type="submit"
                class="inline-flex justify-center border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline"
              >
                Simpan
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditFormSiswa;
