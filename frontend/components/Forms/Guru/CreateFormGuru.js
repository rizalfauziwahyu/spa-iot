import React from "react";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { userService } from "services/user.service";

function CreateFormGuru() {
  const router = useRouter();
  const { register, handleSubmit } = useForm();

  const onSubmit = async (data) => {
    const formData = new FormData();
    if (data.file[0]) {
      formData.append("file", data.file[0]);
    }

    formData.append("username", data.username);
    formData.append("password", data.password);
    formData.append("name", data.name);
    formData.append("gender", data.gender);
    formData.append("address", data.address);
    formData.append("phone", data.phone);
    formData.append("role", "TEACHER");
  

    const res = await userService.createUser(formData);
    if (res.message === "created") {
      router.push("/admin/guru");
    }
  };

  return (
    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg mt-16">
      <div className="px-6">
        <div className="rounded-t mb-0 px-4 py-3 border-0">
          <div className="flex flex-wrap items-center">
            <div className="relative w-full max-w-full flex-grow flex-1">
              <h3 className="font-semibold text-lg text-blueGray-700">
                Tambahkan Data Guru
              </h3>
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div class="overflow-hidden sm:rounded-md">
            <div class="px-4 py-5 sm:p-6">
              <div class="grid grid-cols-6 gap-6">
                <div class="col-span-6 sm:col-span-3">
                  <label
                    for="username"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Username
                  </label>
                  <input
                    type="text"
                    name="username"
                    id="username"
                    autocomplete="given-name"
                    {...register("username", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-3">
                  <label
                    for="password"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    autocomplete="family-name"
                    {...register("password", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-4">
                  <label
                    for="name"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Nama Lengkap
                  </label>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    autocomplete="name"
                    {...register("name", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-4">
                  <label
                    for="file"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Upload Foto Profil
                  </label>
                  <input
                    type="file"
                    name="file"
                    id="file"
                    autocomplete="file"
                    {...register("file")}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-3">
                  <label
                    for="gender"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Jenis Kelamin
                  </label>
                  <select
                    id="gender"
                    name="gender"
                    autocomplete="gender"
                    {...register("gender", { required: true })}
                    class="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  >
                    <option value="">Pilih Jenis Kelamin</option>
                    <option value="MALE">Laki-laki</option>
                    <option value="FEMALE">Perempuan</option>
                  </select>
                </div>

                <div class="col-span-6">
                  <label
                    for="address"
                    class="block text-sm font-medium text-gray-700"
                  >
                    Alamat
                  </label>
                  <input
                    type="text"
                    name="address"
                    id="address"
                    autocomplete="address"
                    {...register("address", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                  <label
                    for="phone"
                    class="block text-sm font-medium text-gray-700"
                  >
                    No HP
                  </label>
                  <input
                    type="text"
                    name="phone"
                    id="phone"
                    autocomplete="phone"
                    {...register("phone", { required: true })}
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

              </div>
            </div>
            <div class="bg-blue-50 px-4 py-3 text-right sm:px-6">
              <button
                htmlType="submit"
                class="inline-flex justify-center border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline"
              >
                Simpan
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default CreateFormGuru;
