import React from "react";

const TableBody = ({ tableData, columns }) => {
  return (
    <tbody>
      {tableData.map((data, index) => {
        return (
          <tr key={`${data.id}-${index}`}>
            {columns.map(({ accessor }) => {
              const tData = data[accessor] ? data[accessor] : "——";
              return (
                <td
                  key={accessor}
                  className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                >
                  {tData}
                </td>
              );
            })}
          </tr>
        );
      })}
    </tbody>
  );
};

export default TableBody;
