import React from "react";

const Pagination = ({ nPages, currentPage, setCurrentPage }) => {
  const pageNumbers = [...Array(nPages + 1).keys()].slice(1);

  const nextPage = () => {
    if (currentPage !== nPages) setCurrentPage(currentPage + 1);
  };
  const prevPage = () => {
    if (currentPage !== 1) setCurrentPage(currentPage - 1);
  };
  return (
    <div className="flex rounded-lg justify-center">
      <button
        onClick={prevPage}
        className="h-12 border-2 border-r-0 border-lih
           px-4 rounded-l-lg hover:bg-lightBlue-500 hover:text-white"
      >
        Prev
      </button>
      {pageNumbers.map((pg, i) => (
        <button
          key={i}
          onClick={() => setCurrentPage(pg)}
          className={`h-12 border-2 border-r-0 border-indigo-600
           w-12 ${currentPage === pg && "bg-lightBlue-500 text-white"}`}
        >
          {pg}
        </button>
      ))}
      <button
        onClick={nextPage}
        className="h-12 border-2  border-indigo-600
           px-4 rounded-r-lg hover:bg-lightBlue-500 hover:text-white"
      >
        Next
      </button>
    </div>
  );
};

export default Pagination;
