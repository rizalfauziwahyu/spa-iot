import React, { useEffect, useState } from "react";
import TableBody from "./TableBody";
import TableHead from "./TableHead";
import { useSortableTable } from "helpers/useSortableTable";
import Pagination from "./Pagination";

const Table = ({ color = "light", caption, columns, data }) => {
  const [tableData, handleSorting] = useSortableTable(data, columns);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage] = useState(50);

  const indexOfLastRecord = currentPage * recordsPerPage;
  const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
  const currentRecords = tableData.slice(indexOfFirstRecord, indexOfLastRecord);
  const nPages = Math.ceil(tableData.length / recordsPerPage);

  return (
    <>
      <div
        className={
          "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded " +
          (color === "light" ? "bg-white" : "bg-blueGray-700 text-white")
        }
      >
        <div className="rounded-t mb-0 px-4 py-3 border-0">
          <div className="flex flex-wrap items-center">
            <div className="relative w-full px-4 max-w-full flex-grow flex-1">
              <h3
                className={
                  "font-semibold text-lg " +
                  (color === "light" ? "text-blueGray-700" : "text-white")
                }
              >
                {caption}
              </h3>
            </div>
          </div>
        </div>
        <div className="block w-full overflow-x-auto">
          <table className="items-center w-full bg-transparent border-collapse">
            <TableHead {...{ columns, handleSorting }} />
            <TableBody tableData={currentRecords} columns={columns} />
          </table>
        </div>
      </div>
      <Pagination nPages={nPages} currentPage={currentPage} setCurrentPage={setCurrentPage} />
    </>
  );
};

export default Table;
