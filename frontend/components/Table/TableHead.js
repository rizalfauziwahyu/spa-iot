import React, { useState } from "react";

const TableHead = ({ columns, handleSorting, color = "light" }) => {
  const [sortField, setSortField] = useState("");
  const [order, setOrder] = useState("asc");

  const handleSortingChange = (accessor) => {
    const sortOrder =
      accessor === sortField && order === "asc" ? "desc" : "asc";
    setSortField(accessor);
    setOrder(sortOrder);
    handleSorting(accessor, sortOrder);
  };

  return (
    <thead>
      <tr>
        {columns.map(({ label, accessor, sortable }, i) => {
          const cl = sortable
            ? sortField === accessor && order === "asc"
              ? "fas fa-solid fa-sort-up"
              : sortField === accessor && order === "desc"
              ? "fas fa-solid fa-sort-down"
              : "fas fa-solid fa-sort"
            : "";
          return (
            <th
              className={
                "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                (color === "light"
                  ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                  : "bg-blueGray-600 text-blueGray-200 border-blueGray-500 ")
              }
              key={accessor}
              onClick={sortable ? () => handleSortingChange(accessor) : null}
            >
              {label} <i className={cl}></i>
            </th>
          );
        })}
      </tr>
    </thead>
  );
};

export default TableHead;
