import React, { useEffect } from "react";
import { createPopper } from "@popperjs/core";
import { useRouter } from "next/router";
import { userService } from "services/user.service";
import { kelasService } from "services/kelas.service";

const TableDropdown = ({ showHref, editHref, deleteHref, id }) => {
  const router = useRouter();

  const onClickHandler = () => {
    if (deleteHref.match("users")) {
      return userService
        .deleteById(id)
        .then((res) => {
          console.log(res);
          router.reload(window.location.pathname)
        })
        .catch((err) => {
          console.log(err);
        });
    } else if (deleteHref.match("classrooms")) {
      return kelasService
        .destroy(id)
        .then((res) => {
          console.log(res);
          router.reload(window.location.pathname)
        })
        .catch((err) => {
          console.log(err);
        });
    } else if (deleteHref.match("teachers")) {
      return userService
        .deleteById(id)
        .then((res) => {
          console.log(res);
          router.reload(window.location.pathname)
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  // dropdown props
  const [dropdownPopoverShow, setDropdownPopoverShow] = React.useState(false);
  const btnDropdownRef = React.createRef();
  const popoverDropdownRef = React.createRef();
  const openDropdownPopover = () => {
    createPopper(btnDropdownRef.current, popoverDropdownRef.current, {
      placement: "left-start",
    });
    setDropdownPopoverShow(true);
  };
  const closeDropdownPopover = () => {
    setDropdownPopoverShow(false);
  };
  return (
    <>
      <a
        className="text-blueGray-500 py-1 px-3"
        href="#pablo"
        ref={btnDropdownRef}
        onClick={(e) => {
          e.preventDefault();
          dropdownPopoverShow ? closeDropdownPopover() : openDropdownPopover();
        }}
      >
        <i className="fas fa-ellipsis-v"></i>
      </a>
      <div
        ref={popoverDropdownRef}
        className={
          (dropdownPopoverShow ? "block " : "hidden ") +
          "bg-white text-base z-50 float-left py-2 list-none text-left rounded shadow-lg min-w-48"
        }
      >
        <a
          href={showHref ? showHref : "#"}
          className={
            "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-blueGray-700"
          }
          onClick={(e) => {
            e.preventDefault();
            router.push(showHref);
          }}
        >
          <i className="fas fa-eye"></i> Lihat
        </a>
        <a
          href={editHref ? editHref : "#"}
          className={
            "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-blueGray-700"
          }
          onClick={(e) => {
            e.preventDefault();
            router.push(editHref);
          }}
        >
          <i className="fas fa-edit"></i> Edit
        </a>
        <a
          href={deleteHref ? deleteHref : "#"}
          className={
            "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-blueGray-700"
          }
          onClick={(e) => {
            e.preventDefault();
            onClickHandler();
          }}
        >
          <i className="fas fa-trash"></i> Hapus
        </a>
      </div>
    </>
  );
};

export default TableDropdown;
