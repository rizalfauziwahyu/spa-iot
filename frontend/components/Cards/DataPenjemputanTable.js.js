import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

import { pickupService } from "services/pickup.service";
import TableDropdown from "components/Dropdowns/TableDropdown.js";

export default function DataPenjemputanTable({ color, heading }) {
  const [data, setData] = useState([]);

  const columns = [
    {
      title: "Siswa",
      dataIndex: "siswa",
      key: "siswa",
    },
    {
      title: "Kelas",
      dataIndex: "kelas",
      key: "kelas",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
    },
    {
      title: "Tanggal",
      dataIndex: "pickups",
      key: "pickups",
    },
    {
      title: "Waktu",
      dataIndex: "pickupstime",
      key: "pickupstime",
    },
  ];

  useEffect(() => {
    (async () => {
      const result = await pickupService.index();
      setData(result.data);
    })();
  }, []);

  return (
    <>
      <div
        className={
          "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded " +
          (color === "light" ? "bg-white" : "bg-blueGray-700 text-white")
        }
      >
        <div className="rounded-t mb-0 px-4 py-3 border-0">
          <div className="flex flex-wrap items-center">
            <div className="relative w-full px-4 max-w-full flex-grow flex-1">
              <h3
                className={
                  "font-semibold text-lg " +
                  (color === "light" ? "text-blueGray-700" : "text-white")
                }
              >
                {heading}
              </h3>
            </div>
          </div>
        </div>
        <div className="block w-full overflow-x-auto">
          {/* Projects table */}
          <table className="items-center w-full bg-transparent border-collapse">
            <thead>
              <tr>
                {columns.map((column, i) => {
                  return (
                    <th
                      className={
                        "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                        (color === "light"
                          ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                          : "bg-blueGray-600 text-blueGray-200 border-blueGray-500")
                      }
                      key={column.key}
                    >
                      {column.title}
                    </th>
                  );
                })}
              </tr>
            </thead>
            <tbody>
              {data.map((child, i) => {
                return (
                  <tr key={i}>
                    <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center">
                      <span
                        className={
                          "ml-3 font-bold " +
                          +(color === "light"
                            ? "text-blueGray-600"
                            : "text-white")
                        }
                      >
                        {child.user ? child.user.name : ""}
                      </span>
                    </th>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {child.user ? <>{child.user.classroom ? child.user.classroom.title : ""}</> : ""}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {child.status === "PICKUP"
                        ? "SUDAH DIJEMPUT"
                        : "BELUM DIJEMPUT"}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {child.date? new Date(child.date).toLocaleDateString() : ""}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {child.date? new Date(child.date).toLocaleTimeString() : ""}
                    </td>

                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}

DataPenjemputanTable.defaultProps = {
  color: "light",
};

DataPenjemputanTable.propTypes = {
  color: PropTypes.oneOf(["light", "dark"]),
};
