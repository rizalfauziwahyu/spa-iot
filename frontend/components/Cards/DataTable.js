import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

// components
import { kelasService } from "services/kelas.service";

import TableDropdown from "components/Dropdowns/TableDropdown.js";

export default function DataTable({ color, heading }) {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Walikelas",
      dataIndex: "homeroom",
      key: "homeroom",
    },
    {
      title: "Jumlah Siswa",
      dataIndex: "students",
      key: "students",
    },
  ];

  useEffect(() => {
    (async () => {
      const result = await kelasService.index();
      setData(result.data);
      setIsLoading(false);
    })();
  }, [setData]);

  return (
    <>
      {isLoading ? (
        ""
      ) : (
        <div
          className={
            "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded " +
            (color === "light" ? "bg-white" : "bg-blueGray-700 text-white")
          }
        >
          <div className="rounded-t mb-0 px-4 py-3 border-0">
            <div className="flex flex-wrap items-center">
              <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                <h3
                  className={
                    "font-semibold text-lg " +
                    (color === "light" ? "text-blueGray-700" : "text-white")
                  }
                >
                  {heading}
                </h3>
              </div>
            </div>
          </div>
          <div className="block w-full overflow-x-auto">
            {/* Projects table */}
            <table className="items-center w-full bg-transparent border-collapse">
              <thead>
                <tr>
                  {columns.map((column, i) => {
                    return (
                      <th
                        className={
                          "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                          (color === "light"
                            ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                            : "bg-blueGray-600 text-blueGray-200 border-blueGray-500")
                        }
                        key={column.key}
                      >
                        {column.title}
                      </th>
                    );
                  })}
                  <th
                    className={
                      "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                      (color === "light"
                        ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                        : "bg-blueGray-600 text-blueGray-200 border-blueGray-500")
                    }
                  ></th>
                </tr>
              </thead>
              <tbody>
                {data.map((child, i) => {
                  return (
                    <tr key={i}>
                      <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center">
                        <span
                          className={
                            "ml-3 font-bold " +
                            +(color === "light"
                              ? "text-blueGray-600"
                              : "text-white")
                          }
                        >
                          {child.title}
                        </span>
                      </th>
                      <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                        {child.teacher ? child.teacher.name : ""}
                      </td>
                      <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                        {child.User ? child.User.length : ""}
                      </td>
                      <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-right">
                        <TableDropdown
                          showHref={`/admin/show/kelas?id=${child.id}`}
                          editHref={`/admin/edit/kelas?id=${child.id}`}
                          deleteHref="classrooms"
                          id={child.id}
                        />
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      )}
    </>
  );
}

DataTable.defaultProps = {
  color: "light",
};

DataTable.propTypes = {
  color: PropTypes.oneOf(["light", "dark"]),
};
