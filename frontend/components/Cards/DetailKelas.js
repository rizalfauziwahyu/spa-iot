import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { userService } from "services/user.service";
import { kelasService } from "services/kelas.service";
import SiswaTable from "./SiswaTable";
import SiswaKelasTable from "./SiswaKelasTable";

function DetailKelas() {
  const router = useRouter();
  const { id } = router.query;
  const [detail, setDetail] = useState([]);

  useEffect(() => {
    kelasService.show(id).then((res) => {
      setDetail(res.data);
    });
  }, [setDetail]);

  return (
    <>
      {detail ? (
        <>
          <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg mt-16">
            <div className="px-6">
              <div class="overflow-hidden bg-white sm:rounded-lg">
                <div class="px-4 py-5 sm:px-6">
                  <h3 class="text-lg font-medium leading-6 text-gray-900">
                    Informasi Data Kelas
                  </h3>
                </div>
                <div class="border-t border-gray-200">
                  <dl>
                    <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt class="text-sm font-medium text-gray-500">
                        Nama Kelas
                      </dt>
                      <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                        {detail.title}
                      </dd>
                    </div>
                    <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt class="text-sm font-medium text-gray-500">
                        Kode Suara
                      </dt>
                      <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                        {detail.soundNumber}
                      </dd>
                    </div>
                    <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt class="text-sm font-medium text-gray-500">
                        Wali Kelas
                      </dt>
                      <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                        {detail.teacher ? detail.teacher.name : ""}
                      </dd>
                    </div>
                    <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt class="text-sm font-medium text-gray-500">
                        Jumlah Siswa
                      </dt>
                      <dd class="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                        {detail.User ? detail.User.length : 0}
                      </dd>
                    </div>
                  </dl>
                </div>
              </div>
            </div>
          </div>
          <SiswaKelasTable heading="Daftar Siswa" dataSiswa={detail.User} />
        </>
      ) : (
        <h3 class="text-lg font-medium leading-6 text-gray-900">Loading...</h3>
      )}
    </>
  );
}

export default DetailKelas;
