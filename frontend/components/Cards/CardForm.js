import React, { useEffect, useState } from 'react';
import { userService } from 'services/user.service';
import { useRouter } from 'next/router';

function CardForm() {
  const router = useRouter();

  const handleSubmit = async event => {
    // Stop the form from submitting and refreshing the page.
    event.preventDefault();

    // Get data from the form.
    const data = {
      username: event.target.username.value,
      password: event.target.password.value,
      name: event.target.name.value,
      gender: event.target.gender.value,
      address: event.target.address.value,
      phone: event.target.phone.value,
      parent: event.target.parent.value,
      phone2: event.target.phone2.value,
    };

    onSubmit(data);
  };

  const onSubmit = data => {
    const { username, password, name, gender, address, phone } = data;
    return userService
      .createUser({ username, password, name, gender, address, phone })
      .then(res => {
        console.log(res);
        router.push('/admin/dashboard');
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg mt-16">
      <div className="px-6">
        <div className="rounded-t mb-0 px-4 py-3 border-0">
          <div className="flex flex-wrap items-center">
            <div className="relative w-full max-w-full flex-grow flex-1">
              <h3 className="font-semibold text-lg text-blueGray-700">Tambahkan Data Siswa</h3>
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit}>
          <div class="overflow-hidden sm:rounded-md">
            <div class="px-4 py-5 sm:p-6">
              <div class="grid grid-cols-6 gap-6">
                <div class="col-span-6 sm:col-span-3">
                  <label for="username" class="block text-sm font-medium text-gray-700">
                    Username
                  </label>
                  <input
                    type="text"
                    name="username"
                    id="username"
                    autocomplete="given-name"
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-3">
                  <label for="password" class="block text-sm font-medium text-gray-700">
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    autocomplete="family-name"
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-4">
                  <label for="name" class="block text-sm font-medium text-gray-700">
                    Nama Lengkap
                  </label>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    autocomplete="name"
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-3">
                  <label for="gender" class="block text-sm font-medium text-gray-700">
                    Jenis Kelamin
                  </label>
                  <select
                    id="gender"
                    name="gender"
                    autocomplete="gender"
                    class="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  >
                    <option value="male">Laki-laki</option>
                    <option value="female">Perempuan</option>
                  </select>
                </div>

                <div class="col-span-6">
                  <label for="address" class="block text-sm font-medium text-gray-700">
                    Alamat
                  </label>
                  <input
                    type="text"
                    name="address"
                    id="address"
                    autocomplete="address"
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                  <label for="phone" class="block text-sm font-medium text-gray-700">
                    No HP
                  </label>
                  <input
                    type="text"
                    name="phone"
                    id="phone"
                    autocomplete="phone"
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                  <label for="parent" class="block text-sm font-medium text-gray-700">
                    Nama Orang Tua / Wali
                  </label>
                  <input
                    type="text"
                    name="parent"
                    id="parent"
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>

                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                  <label for="phone2" class="block text-sm font-medium text-gray-700">
                    No HP Orang Tua / Wali
                  </label>
                  <input
                    type="text"
                    name="phone2"
                    id="phone2"
                    autocomplete="phone2"
                    class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>
              </div>
            </div>
            <div class="bg-blue-50 px-4 py-3 text-right sm:px-6">
              <button
                type="submit"
                class="inline-flex justify-center border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline"
              >
                Simpan
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default CardForm;
