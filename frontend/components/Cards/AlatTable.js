import React, { useEffect, useState } from "react";
import { userService } from "services/user.service";
import { useRouter } from "next/router";
// components

export default function AlatTable() {
  const router = useRouter();
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    (async () => {
      const result = await userService.getDevice();
      setData(result.data);
      setIsLoading(false);
    })();
  }, []);

  const onClickHandler = async () => {
    setIsLoading(true);
    const body = {};
    if (data.mode === "WRITER") {
      body.mode = "READER";
    } else {
      body.mode = "WRITER";
    }
    const res = await userService.changeDeviceMode(body);
    if (res.message === "updated") {
      refreshData();
    }
  };

  const refreshData = async () => {
    const result = await userService.getDevice();
    setData(result.data);
    setIsLoading(false);
  }

  return (
    <>
      {isLoading ? (
        "Loading..."
      ) : (
        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg mt-16">
          <div className="px-6">
            <div className="text-center mt-12">
              <h3 className="text-xl font-semibold leading-normal mb-2 text-blueGray-700 mb-2">
                Perangkat RFID
              </h3>

              <div className="text-sm leading-normal mt-0 mb-2 text-blueGray-700 font-bold uppercase">
                Status Alat : {data.mode}
              </div>
            </div>
            <div class="bg-blue-50 px-4 py-3 text-center sm:px-6">

              <button
                type="button"
                onClick={onClickHandler}
                class="inline-flex justify-center border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline"
              >
                Ganti Mode
              </button>
            </div>
         
          </div>
        </div>
      )}
    </>
  );
}
