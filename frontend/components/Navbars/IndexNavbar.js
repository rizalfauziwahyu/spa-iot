import React from "react";
import Link from "next/link";
// components

import { userService } from "../../services/user.service";

import IndexDropdown from "components/Dropdowns/IndexDropdown.js";

function NavbarButton(props) {
  if (props.auth) {
    return (
      <Link
        className="bg-blueGray-700 text-white active:bg-blueGray-600 text-xs font-bold uppercase px-4 py-2 rounded shadow hover:shadow-lg outline-none focus:outline-none lg:mr-1 lg:mb-0 ml-3 mb-3 ease-linear transition-all duration-150"
        href="/admin/dashboard"
      >
        Dashboard
      </Link>
    );
  } else {
    return (
      <Link
        className="bg-blueGray-700 text-white active:bg-blueGray-600 text-xs font-bold uppercase px-4 py-2 rounded shadow hover:shadow-lg outline-none focus:outline-none lg:mr-1 lg:mb-0 ml-3 mb-3 ease-linear transition-all duration-150"
        href="/auth/login"
      >
        Login
      </Link>
    );
  }
}

export default function Navbar(props) {
  const [navbarOpen, setNavbarOpen] = React.useState(false);
  return (
    <>
      <nav className="top-0 fixed z-50 w-full flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg bg-white shadow">
        <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
          <div className="w-full relative flex justify-between ">
            <Link href="/">
              <a
                className="text-blueGray-700 text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase"
                href="#pablo"
              >
                <img
                  alt="logo"
                  className="items-start"
                  src="/img/brand/favicon.png"
                />
              </a>
            </Link>
            <div
              className={
                "items-end bg-white lg:bg-opacity-0 lg:shadow-none"
              }
              id="example-navbar-warning"
            >
              <ul className="list-none">
                <li className="flex justify-items-center mt-5">
                  <NavbarButton auth={userService.authorization} />
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
}
