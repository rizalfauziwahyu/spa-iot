import React from "react";

// components

import CardStats from "components/Cards/CardStats.js";

export default function HeaderStats() {
  return (
    <>
      {/* Header */}
      <div className="relative bg-blueGray-800 md:pt-32 pb-32 pt-12">
        <div className="px-4 md:px-10 mx-auto w-full">
          <div>
            {/* Card stats */}
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="Tambahkan Data"
                  statTitle="WALI KELAS"
                  statArrow=""
                  statPercent=""
                  statPercentColor="text-emerald-500"
                  statDescripiron=""
                  statIconName="fas fa-plus"
                  statIconColor="bg-red-500"
                  statHref="/admin/create/guru"
                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="Tambahkan Data"
                  statTitle="KELAS"
                  statArrow=""
                  statPercent=""
                  statPercentColor="text-red-500"
                  statDescripiron=""
                  statIconName="fas fa-plus"
                  statIconColor="bg-orange-500"
                  statHref="/admin/create/kelas"
                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="Tambahkan Data"
                  statTitle="SISWA"
                  statArrow=""
                  statPercent=""
                  statPercentColor="text-orange-500"
                  statDescripiron=""
                  statIconName="fas fa-plus"
                  statIconColor="bg-pink-500"
                  statHref="/admin/rfidbaru"
                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle=""
                  statTitle="LOG PENJEMPUTAN"
                  statArrow=""
                  statPercent=""
                  statPercentColor="text-emerald-500"
                  statDescripiron=""
                  statIconName="fas fa-eye"
                  statIconColor="bg-lightBlue-500"
                  statHref="/admin/penjemputan"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
