import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../redux/features/authSlice";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";

const persistConfig = {
  key: "root",
  storage,
};

const persistedAuth = persistReducer(persistConfig, authReducer);

export const store = configureStore({
  reducer: {
    auth: persistedAuth,
  },
  middleware: [thunk],
});

export const persistor = persistStore(store);
