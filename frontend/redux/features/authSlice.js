import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { userService } from "services/user.service";

const initialState = {
  user: null,
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};

const baseUrl = "http://localhost:3000/api/v1";

export const LoginUser = createAsyncThunk("login", async (user, thunkAPI) => {
  try {
    const response = await userService.login(user.username, user.password);

    return response;
  } catch (error) {
    if (error.response) {
      const message = error.response.data.message;
      return thunkAPI.rejectWithValue(message);
    }
  }
});

export const LogOut = createAsyncThunk("logout", async () => {
  await userService.logout();
});

export const getUser = createAsyncThunk("getUser", async (_, thunkAPI) => {
  try {
    const response = await userService.getMe();

    return response;
  } catch (error) {
    if (error.response) {
      const message = error.response.data.message;
      return thunkAPI.rejectWithValue(message);
    }
  }
});

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    reset: (state) => initialState,
  },
  extraReducers: (builder) => {
    builder.addCase(LoginUser.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(LoginUser.fulfilled, (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;
      state.user = action.payload.data;
    });
    builder.addCase(LoginUser.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload.message;
    });

    builder.addCase(LogOut.fulfilled, (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;
      state.user = {};
    });

    builder.addCase(getUser.fulfilled, (state, action) => {
      state.user = action.payload.data;
    });
    builder.addCase(getUser.rejected, (state, action) => {
      state.isError = true;
      state.message = action.payload.message;
      state.user = null;
    });
  },
});

export const { reset } = authSlice.actions;
export default authSlice.reducer;
