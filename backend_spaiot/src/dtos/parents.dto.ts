import { IsNumber, IsString } from 'class-validator';

export class CreateParentDto {
  @IsNumber()
  public userId: number;

  @IsString()
  public name: string;

  @IsString()
  public phone: string;
}
