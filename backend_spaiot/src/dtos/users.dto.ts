import { IsEmpty, IsNegative, IsNumber, IsString } from 'class-validator';

export class CreateRfidDto {
  @IsString()
  public rfid: string;
}

export class CreateUserDto {
  @IsString()
  public username: string;

  @IsString()
  public password: string;

  @IsString()
  public name: string;

  @IsString()
  public address?: string;

  @IsString()
  public gender: string;

  @IsString()
  public phone: string;

  @IsString()
  public parent?: string;

  @IsString()
  public parentPhone?: string;

  @IsString()
  public photo?: string;

  @IsString()
  public file?: string;

  @IsString()
  public classroomId?: string;

  @IsString()
  public classroom?: string;

  @IsString()
  public rfid?: string;

  @IsString()
  public role?: string;

  @IsString()
  public soundNumber?: string;
}

export class LoginUserDto {
  @IsString()
  public username: string;

  @IsString()
  public password: string;
}
