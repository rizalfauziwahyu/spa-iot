import { IsNumber, IsString } from 'class-validator';

export class CreateClassroomDto {
  @IsNumber()
  public teacherId: number;

  @IsString()
  public title: string;

  @IsNumber()
  public soundNumber?: number;
}
