import { IsNumber, IsString } from 'class-validator';

export class CreatePickupDto {
  @IsNumber()
  public userId?: number;

  @IsString()
  public rfid?: string;
}
