import { Router } from 'express';
import UsersController from '@controllers/users.controller';
import { CreateRfidDto, CreateUserDto } from '@dtos/users.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import { upload } from '@/utils/uploader';
import authMiddleware from '@/middlewares/auth.middleware';

class UsersRoute implements Routes {
  public path = '/api/v1/users';
  public router = Router();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, authMiddleware, this.usersController.getUsers);
    this.router.get(`${this.path}/new-rfid`, authMiddleware, this.usersController.getAllNewRfid);
    this.router.post(`${this.path}/new-rfid`, validationMiddleware(CreateRfidDto, 'body'), this.usersController.createRfid);
    this.router.get(`${this.path}/device`, this.usersController.getDevice);
    this.router.put(`${this.path}/device`, authMiddleware, this.usersController.updateDeviceMode);
    this.router.get(`${this.path}/teachers`, authMiddleware, this.usersController.getTeachers);
    this.router.get(`${this.path}/available-teachers`, authMiddleware, this.usersController.getAvailableTeachers);
    this.router.get(`${this.path}/me`, authMiddleware, this.usersController.getMe);
    this.router.get(`${this.path}/:id(\\d+)`, authMiddleware, this.usersController.getUserById);
    this.router.post(`${this.path}`, authMiddleware, upload, validationMiddleware(CreateUserDto, 'body', true), this.usersController.createUser);
    this.router.put(
      `${this.path}/:id(\\d+)`,
      authMiddleware,
      upload,
      validationMiddleware(CreateUserDto, 'body', true),
      this.usersController.updateUser,
    );
    this.router.delete(`${this.path}/:id(\\d+)`, authMiddleware, this.usersController.deleteUser);
  }
}

export default UsersRoute;
