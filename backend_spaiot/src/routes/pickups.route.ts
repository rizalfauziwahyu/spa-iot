import { Router } from 'express';
import PickupsController from '@/controllers/pickups.controller';
import { CreatePickupDto } from '@/dtos/pickups.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import authMiddleware from '@/middlewares/auth.middleware';

class PickupsRoute implements Routes {
  public path = '/api/v1/pickups';
  public router = Router();
  public pickupsController = new PickupsController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, authMiddleware, this.pickupsController.getPickups);
    this.router.get(`${this.path}/:id(\\d+)`, authMiddleware, this.pickupsController.getPickupById);
    this.router.get(`${this.path}/user/:id(\\d+)`, authMiddleware, this.pickupsController.getPickupsByUserId);
    this.router.get(`${this.path}/classroom/:id(\\d+)`, authMiddleware, this.pickupsController.getPickupsByClassroomId);
    this.router.post(`${this.path}`, validationMiddleware(CreatePickupDto, 'body', true), this.pickupsController.pickupRfid);
    this.router.delete(`${this.path}/:id(\\d+)`, authMiddleware, this.pickupsController.deletePickup);
  }
}

export default PickupsRoute;
