import { Router } from 'express';
import ClassroomsController from '@/controllers/classrooms.controller';
import { CreateClassroomDto } from '@/dtos/classrooms.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import authMiddleware from '@/middlewares/auth.middleware';

class ClassroomsRoute implements Routes {
  public path = '/api/v1/classrooms';
  public router = Router();
  public classroomsController = new ClassroomsController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, authMiddleware, this.classroomsController.getClassrooms);
    this.router.get(`${this.path}/:id(\\d+)`, authMiddleware, this.classroomsController.getClassroomById);
    this.router.post(
      `${this.path}`,
      authMiddleware,
      validationMiddleware(CreateClassroomDto, 'body', true),
      this.classroomsController.createClassroom,
    );
    this.router.put(
      `${this.path}/:id(\\d+)`,
      authMiddleware,
      validationMiddleware(CreateClassroomDto, 'body', true),
      this.classroomsController.updateClassroom,
    );
    this.router.delete(`${this.path}/:id(\\d+)`, authMiddleware, this.classroomsController.deleteClassroom);
  }
}

export default ClassroomsRoute;
