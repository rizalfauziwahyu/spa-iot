import { hash } from 'bcrypt';
import { Device, DeviceMode, PrismaClient, Role, User } from '@prisma/client';
import { CreateUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { isEmpty, exclude } from '@utils/util';
import path from 'path';
import fs from 'fs';

class UserService {
  public users = new PrismaClient().user;
  public device = new PrismaClient().device;
  public prisma = new PrismaClient();

  public async findAllUser(): Promise<User[]> {
    const allUser: User[] = await this.users.findMany({
      where: {
        role: Role.STUDENT,
      },
      select: {
        id: true,
        username: true,
        name: true,
        photo: true,
        address: true,
        gender: true,
        phone: true,
        role: true,
        rfid: true,
        parent: true,
        classroom: true,
        homeroom: true,
        createdAt: true,
        updatedAt: true,
      },
      orderBy: {
        createdAt: 'desc',
      },
    });
    return allUser;
  }

  public async findAllTeacher(): Promise<User[]> {
    const allTeacher: User[] = await this.users.findMany({
      where: {
        role: Role.TEACHER,
      },
      select: {
        id: true,
        username: true,
        name: true,
        photo: true,
        address: true,
        gender: true,
        phone: true,
        role: true,
        rfid: true,
        classroom: true,
        homeroom: true,
        createdAt: true,
        updatedAt: true,
      },
      orderBy: {
        createdAt: 'desc',
      },
    });
    return allTeacher;
  }

  public async findMe(userId: number): Promise<User> {
    if (isEmpty(userId)) throw new HttpException(400, 'UserId is empty');

    const findUser: User = await this.users.findUnique({
      where: { id: userId },
      include: { parent: true, classroom: true, homeroom: true, Pickup: true },
    });
    if (!findUser) throw new HttpException(409, "User doesn't exist");

    return findUser;
  }

  public async findUserById(userId: number): Promise<User> {
    if (isEmpty(userId)) throw new HttpException(400, 'UserId is empty');

    const findUser: User = await this.users.findUnique({
      where: { id: userId },
      include: { parent: true, classroom: true, homeroom: true, Pickup: true },
    });
    if (!findUser) throw new HttpException(409, "User doesn't exist");

    return findUser;
  }

  public async findAvailableTeacher(): Promise<User[]> {
    const findUser: User[] = await this.users.findMany({
      where: {
        homeroom: {
          is: null,
        },
        role: Role.TEACHER,
      },
    });

    return findUser;
  }

  public async createRfid(uniqueRfid: string): Promise<User> {
    console.log(uniqueRfid);

    const findUser: User = await this.users.findUnique({ where: { rfid: uniqueRfid } });

    if (findUser) {
      throw new HttpException(409, `Kartru RFID ${uniqueRfid} ini sudah digunakan!`);
    }

    const createNewRfid = await this.users.create({
      data: {
        rfid: uniqueRfid,
      },
    });

    return createNewRfid;
  }

  public async createUser(userData: CreateUserDto): Promise<User> {
    console.log(userData);
    if (isEmpty(userData)) throw new HttpException(400, 'userData is empty');

    const findUser: User = await this.users.findUnique({ where: { username: userData.username }, include: { classroom: true, parent: true } });

    if (findUser) {
      throw new HttpException(409, `This username ${userData.username} already exists`);
    }

    const hashedPassword = await hash(userData.password, 10);

    const { parent, parentPhone, classroomId } = userData;
    exclude(userData, 'parent', 'parentPhone', 'classroomId');

    let createUserData: User;
    if (classroomId) {
      createUserData = await this.users.create({
        data: { ...userData, password: hashedPassword, classroom: { connect: { id: Number(classroomId) } } },
      });
    } else {
      createUserData = await this.users.create({
        data: { ...userData, password: hashedPassword },
      });
    }

    if (parent && parentPhone) {
      await this.prisma.parent.create({
        data: {
          userId: createUserData.id,
          name: parent,
          phone: parentPhone,
        },
      });

      createUserData = await this.users.findUnique({ where: { id: createUserData.id }, include: { parent: true, classroom: true } });
    }

    exclude(createUserData, 'password');
    return createUserData;
  }

  public async updateUser(userId: number, userData: any): Promise<User> {
    console.log(userData);
    if (isEmpty(userData)) throw new HttpException(400, 'userData is empty');

    const findUser: User = await this.users.findUnique({ where: { id: userId }, include: { parent: true, classroom: true } });
    if (!findUser) throw new HttpException(409, "User doesn't exist");

    const { parent, parentPhone } = userData;
    exclude(userData, 'parent', 'parentPhone', 'file');
    if (userData.soundNumber) userData.soundNumber = Number(userData.soundNumber);
    if (userData.classroomId) userData.classroomId = Number(userData.classroomId);

    console.log(userData);

    if (findUser.photo) {
      const filePath = `${path.join(__dirname, '../../', findUser.photo)}`;
      console.log(filePath);
      fs.unlinkSync(filePath);
    }

    let updateUserData: User;

    if (userData.password) {
      const hashedPassword = await hash(userData.password, 10);
      updateUserData = await this.users.update({
        where: { id: userId },
        data: { ...userData, password: hashedPassword },
      });
    } else {
      updateUserData = await this.users.update({ where: { id: userId }, data: { ...userData } });
    }

    if (parent && parentPhone) {
      await this.prisma.parent.upsert({
        where: { userId: findUser.id },
        create: {
          userId: findUser.id,
          name: parent,
          phone: parentPhone,
        },
        update: {
          name: parent,
          phone: parentPhone,
        },
      });
    }

    updateUserData = await this.users.findUnique({ where: { id: updateUserData.id }, include: { parent: true, classroom: true } });

    return updateUserData;
  }

  public async deleteUser(userId: number): Promise<User> {
    if (isEmpty(userId)) throw new HttpException(400, "User doesn't existId");

    const findUser: User = await this.users.findUnique({ where: { id: userId } });
    if (!findUser) throw new HttpException(409, "User doesn't exist");

    const deleteUserData = await this.users.delete({ where: { id: userId } });
    return deleteUserData;
  }

  public async findAllNewRfid(): Promise<User[]> {
    const allUser: User[] = await this.users.findMany({
      where: {
        role: Role.STUDENT,
        name: null,
      },
      orderBy: {
        createdAt: 'desc',
      },
    });
    return allUser;
  }

  public async deviceMode(deviceMode: DeviceMode): Promise<Device> {
    const findDevice: Device = await this.device.findFirst();

    if (!findDevice) {
      throw new HttpException(409, `Device tidak tersedia`);
    }

    const updateDeviceMode = await this.device.update({
      where: {
        id: findDevice.id,
      },
      data: {
        mode: deviceMode,
      },
    });

    return updateDeviceMode;
  }

  public async getDevice(): Promise<Device> {
    const findDevice: Device = await this.device.findFirst();

    if (!findDevice) {
      throw new HttpException(409, `Device tidak tersedia`);
    }

    return findDevice;
  }
}

export default UserService;
