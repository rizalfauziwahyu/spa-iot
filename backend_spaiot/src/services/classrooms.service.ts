import { PrismaClient, Classroom } from '@prisma/client';
import { CreateClassroomDto } from '@/dtos/classrooms.dto';
import { HttpException } from '@exceptions/HttpException';
import { isEmpty } from '@utils/util';

class ClassroomService {
  public classrooms = new PrismaClient().classroom;
  public prisma = new PrismaClient();

  public async findAllClassroom(): Promise<Classroom[]> {
    const allClassroom: Classroom[] = await this.classrooms.findMany({
      orderBy: {
        title: 'asc',
      },
      select: {
        id: true,
        title: true,
        User: true,
        teacher: true,
        soundNumber: true,
        createdAt: true,
        updatedAt: true,
        teacherId: true,
      },
    });
    return allClassroom;
  }

  public async findClassroomById(classroomId: number): Promise<Classroom> {
    if (isEmpty(classroomId)) throw new HttpException(400, 'classroomId is empty');

    const findClassroom: Classroom = await this.classrooms.findUnique({
      where: { id: classroomId },
      include: { teacher: true, User: true },
    });
    if (!findClassroom) throw new HttpException(409, "classroom doesn't exist");

    return findClassroom;
  }

  public async createClassroom(classroomData: CreateClassroomDto): Promise<Classroom> {
    if (isEmpty(classroomData)) throw new HttpException(400, 'classroomData is empty');

    if (classroomData.teacherId) {
      const findClassroom: Classroom = await this.classrooms.findUnique({ where: { teacherId: Number(classroomData.teacherId) } });

      if (findClassroom) throw new HttpException(409, 'teacher is already have a classroom');

      const createClassroom: Classroom = await this.classrooms.create({
        data: { title: classroomData.title, soundNumber: classroomData.soundNumber, teacher: { connect: { id: Number(classroomData.teacherId) } } },
      });
      return createClassroom;
    } else {
      const createClassroom: Classroom = await this.classrooms.create({
        data: { title: classroomData.title, soundNumber: classroomData.soundNumber },
      });

      return createClassroom;
    }
  }

  public async updateClassroom(classroomId: number, classroomData: CreateClassroomDto): Promise<Classroom> {
    if (isEmpty(classroomData)) throw new HttpException(400, 'classroomData is empty');

    const findClassroom: Classroom = await this.classrooms.findUnique({ where: { id: classroomId } });
    if (!findClassroom) throw new HttpException(409, "classroom doesn't exist");

    if (classroomData.teacherId) {
      const updateClassroomData: Classroom = await this.classrooms.update({
        where: { id: classroomId },
        data: {
          title: classroomData.title,
          soundNumber: classroomData.soundNumber,
          teacher: {
            connect: {
              id: classroomData.teacherId,
            },
          },
        },
        include: {
          teacher: true,
          User: true,
        },
      });

      return updateClassroomData;
    } else {
      const updateClassroomData: Classroom = await this.classrooms.update({
        where: { id: classroomId },
        data: {
          title: classroomData.title,
          soundNumber: classroomData.soundNumber,
          teacherId: null,
        },
      });
      return updateClassroomData;
    }
  }

  public async deleteClassroom(classroomId: number): Promise<Classroom> {
    if (isEmpty(classroomId)) throw new HttpException(400, "classroom doesn't existId");

    const findclassroom: Classroom = await this.classrooms.findUnique({ where: { id: classroomId } });
    if (!findclassroom) throw new HttpException(409, "classroom doesn't exist");

    const deleteClassroomData: Classroom = await this.classrooms.delete({ where: { id: classroomId } });
    return deleteClassroomData;
  }
}

export default ClassroomService;
