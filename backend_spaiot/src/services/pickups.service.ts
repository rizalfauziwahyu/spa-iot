import { PrismaClient, Pickup, Status, User, Classroom } from '@prisma/client';
import { CreatePickupDto } from '@/dtos/pickups.dto';
import { HttpException } from '@exceptions/HttpException';
import { isEmpty } from '@utils/util';

class PickupService {
  public database = new PrismaClient();

  public async findAllPickup(): Promise<Pickup[]> {
    const allPickup: Pickup[] = await this.database.pickup.findMany({
      orderBy: [
        {
          createdAt: 'desc',
        },
      ],
      select: {
        id: true,
        user: {
          select: {
            username: true,
            name: true,
            photo: true,
            gender: true,
            classroom: {
              select: {
                title: true,
                soundNumber: true,
              },
            },
          },
        },
        status: true,
        date: true,
        createdAt: true,
        updatedAt: true,
        userId: true,
      },
    });
    return allPickup;
  }

  public async findPickupById(pickupId: number): Promise<Pickup> {
    if (isEmpty(pickupId)) throw new HttpException(400, 'PickupId is empty');

    const findPickup: Pickup = await this.database.pickup.findUnique({
      where: { id: pickupId },
      select: {
        id: true,
        user: {
          select: {
            username: true,
            name: true,
            photo: true,
            gender: true,
            classroom: {
              select: {
                title: true,
                soundNumber: true,
              },
            },
          },
        },
        status: true,
        date: true,
        createdAt: true,
        updatedAt: true,
        userId: true,
      },
    });
    if (!findPickup) throw new HttpException(409, "Pickup doesn't exist");

    return findPickup;
  }

  public async findAllPickupById(userId: number): Promise<Pickup[]> {
    if (isEmpty(userId)) throw new HttpException(400, 'userId is empty');

    const findPickups: Pickup[] = await this.database.pickup.findMany({
      where: { userId: userId },
      include: {
        user: {
          select: {
            username: true,
            name: true,
            classroom: {
              select: {
                title: true,
                soundNumber: true,
              },
            },
          },
        },
      },
      orderBy: {
        createdAt: 'desc',
      },
    });

    return findPickups;
  }

  public async findAllPickupByClassroomId(classroomId: number): Promise<Classroom> {
    if (isEmpty(classroomId)) throw new HttpException(400, 'classroomId is empty');

    const findClassroom: Classroom = await this.database.classroom.findUnique({
      where: { id: classroomId },
      include: {
        teacher: true,
        User: {
          select: {
            id: true,
            username: true,
            name: true,
            phone: true,
            address: true,
            gender: true,
            rfid: true,
            Pickup: {
              where: {
                createdAt: {
                  gte: new Date(new Date(Date.now()).setHours(0, 1, 0, 0)),
                  lte: new Date(new Date(Date.now()).setHours(23, 59, 0, 0)),
                },
              },
            },
          },
        },
      },
    });
    if (!findClassroom) throw new HttpException(409, "classroom doesn't exist");

    return findClassroom;
  }

  public async pickup(pickupData: CreatePickupDto): Promise<Pickup> {
    if (isEmpty(pickupData)) throw new HttpException(400, 'PickupData is empty');

    const gte = new Date().setHours(0, 0, 0, 0);
    const lt = new Date().setHours(24, 0, 0, 0);

    const findPickup: Pickup = await this.database.pickup.findFirst({
      where: { date: { gte: new Date(gte), lt: new Date(lt) }, userId: pickupData.userId },
    });

    if (findPickup) throw new HttpException(422, 'Anak Sudah Dijemput!!');

    const createPickup: Pickup = await this.database.pickup.create({
      data: {
        date: new Date(Date.now()),
        user: {
          connect: {
            id: pickupData.userId,
          },
        },
        status: Status.PICKUP,
      },
      include: {
        user: {
          include: {
            classroom: {
              select: {
                title: true,
                soundNumber: true,
              },
            },
          },
        },
      },
    });

    return createPickup;
  }

  public async pickupRfid(pickupData: CreatePickupDto): Promise<Pickup> {
    if (isEmpty(pickupData)) throw new HttpException(400, 'PickupData is empty');

    const gte = new Date().setHours(0, 0, 0, 0);
    const lt = new Date().setHours(24, 0, 0, 0);

    const findUser: User = await this.database.user.findUnique({ where: { rfid: pickupData.rfid } });
    if (!findUser) throw new HttpException(423, 'user doesnt exist');

    const findPickup: Pickup = await this.database.pickup.findFirst({
      where: { date: { gte: new Date(gte), lt: new Date(lt) }, userId: findUser.id },
    });

    if (findPickup) throw new HttpException(422, 'user already pickuped');

    const createPickup: Pickup = await this.database.pickup.create({
      data: {
        date: new Date(Date.now()),
        user: {
          connect: {
            id: findUser.id,
          },
        },
        status: Status.PICKUP,
      },
      include: {
        user: {
          select: {
            id: true,
            username: true,
            name: true,
            soundNumber: true,
            classroom: {
              select: {
                title: true,
                soundNumber: true,
              },
            },
          },
        },
      },
    });

    return createPickup;
  }

  public async deletePickup(pickupId: number): Promise<Pickup> {
    if (isEmpty(pickupId)) throw new HttpException(400, "Pickup doesn't existId");

    const findPickup: Pickup = await this.database.pickup.findUnique({ where: { id: pickupId } });
    if (!findPickup) throw new HttpException(409, "Pickup doesn't exist");

    const deletePickupData: Pickup = await this.database.pickup.delete({ where: { id: pickupId } });
    return deletePickupData;
  }
}

export default PickupService;
