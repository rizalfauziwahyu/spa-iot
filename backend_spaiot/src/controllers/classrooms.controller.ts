import { NextFunction, Request, Response } from 'express';
import { Classroom } from '@prisma/client';
import { CreateClassroomDto } from '@/dtos/classrooms.dto';
import ClassroomService from '@/services/classrooms.service';
import { HttpException } from '@/exceptions/HttpException';

class ClassroomsController {
  public classroomService = new ClassroomService();

  public getClassrooms = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllClassroomsData: Classroom[] = await this.classroomService.findAllClassroom();

      res.status(200).json({ data: findAllClassroomsData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getClassroomById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const classroomId = Number(req.params.id);
      const findOneClassroomData: Classroom = await this.classroomService.findClassroomById(classroomId);

      res.status(200).json({ data: findOneClassroomData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public createClassroom = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const classroomData = req.body;
      const createClassroom: Classroom = await this.classroomService.createClassroom(classroomData);

      res.status(201).json({ data: createClassroom, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public updateClassroom = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const classroomId = Number(req.params.id);
      const classroomData: CreateClassroomDto = req.body;

      const updatedClassroomData: Classroom = await this.classroomService.updateClassroom(classroomId, classroomData);
      res.status(200).json({ data: updatedClassroomData, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteClassroom = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const classroomId = Number(req.params.id);
      const deleteClassroomData: Classroom = await this.classroomService.deleteClassroom(classroomId);

      res.status(200).json({ data: deleteClassroomData, message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}

export default ClassroomsController;
