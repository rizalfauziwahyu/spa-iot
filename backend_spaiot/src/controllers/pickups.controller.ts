import { NextFunction, Request, Response } from 'express';
import { Pickup, Classroom } from '@prisma/client';
import PickupService from '@/services/pickups.service';

class PickupsController {
  public pickupService = new PickupService();

  public getPickups = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllPickupsData: Pickup[] = await this.pickupService.findAllPickup();

      res.status(200).json({ data: findAllPickupsData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getPickupsByUserId = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userId = Number(req.params.id);
      const findPickusByUserIdData: Pickup[] = await this.pickupService.findAllPickupById(userId);

      res.status(200).json({ data: findPickusByUserIdData, message: 'findAllByUserId' });
    } catch (error) {
      next(error);
    }
  };

  public getPickupsByClassroomId = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const classroomId = Number(req.params.id);
      const findPickusByClassroomIdData: Classroom = await this.pickupService.findAllPickupByClassroomId(classroomId);

      res.status(200).json({ data: findPickusByClassroomIdData, message: 'findAllByClassroomId' });
    } catch (error) {
      next(error);
    }
  };

  public getPickupById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const PickupId = Number(req.params.id);
      const findOnePickupData: Pickup = await this.pickupService.findPickupById(PickupId);

      res.status(200).json({ data: findOnePickupData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public pickupRfid = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const pickupData = req.body;
      const createPickup: Pickup = await this.pickupService.pickupRfid(pickupData);

      res.status(201).json({ data: createPickup, message: 'Berhasil!!' });
    } catch (error) {
      next(error);
    }
  };

  public deletePickup = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const pickupId = Number(req.params.id);
      const deletePickupData: Pickup = await this.pickupService.deletePickup(pickupId);

      res.status(200).json({ data: deletePickupData, message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}

export default PickupsController;
