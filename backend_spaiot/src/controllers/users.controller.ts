import { NextFunction, Request, Response } from 'express';
import { Device, User } from '@prisma/client';
import { CreateUserDto } from '@dtos/users.dto';
import userService from '@services/users.service';

class UsersController {
  public userService = new userService();

  public getUsers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllUsersData: User[] = await this.userService.findAllUser();

      res.status(200).json({ data: findAllUsersData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getTeachers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllTeachersData: User[] = await this.userService.findAllTeacher();

      res.status(200).json({ data: findAllTeachersData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getAvailableTeachers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllTeachersData: User[] = await this.userService.findAvailableTeacher();

      res.status(200).json({ data: findAllTeachersData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getUserById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userId = Number(req.params.id);
      const findOneUserData: User = await this.userService.findUserById(userId);

      res.status(200).json({ data: findOneUserData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public getMe = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userId = Number(req.user.id);
      const findOneUserData: User = await this.userService.findUserById(userId);

      res.status(200).json({ data: findOneUserData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public createRfid = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { rfid } = req.body;

      const createRfid: User = await this.userService.createRfid(rfid);

      res.status(201).json({ data: createRfid, message: 'Kartu RFID Baru Telah Ditambahkan' });
    } catch (error) {
      next(error);
    }
  };

  public createUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userData = req.body;
      if (req.file) {
        userData.photo = `static/profilePhoto/${req.file.filename}`;
      }

      const createUser: User = await this.userService.createUser(userData);

      res.status(201).json({ data: createUser, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public updateUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userId = Number(req.params.id);
      const userData: CreateUserDto = req.body;
      let updatedUserData: User;
      if (req.file) {
        userData.photo = `static/profilePhoto/${req.file.filename}`;
        updatedUserData = await this.userService.updateUser(userId, userData);
      } else {
        updatedUserData = await this.userService.updateUser(userId, userData);
      }
      res.status(200).json({ data: updatedUserData, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userId = Number(req.params.id);
      const deleteUserData: User = await this.userService.deleteUser(userId);

      res.status(200).json({ data: deleteUserData, message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };

  public getAllNewRfid = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const findAllUsersData: User[] = await this.userService.findAllNewRfid();

      res.status(200).json({ data: findAllUsersData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getDevice = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const device: Device = await this.userService.getDevice();
      res.status(200).json({ data: device, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public updateDeviceMode = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { mode } = req.body;

      const  updatedDeviceMode = await this.userService.deviceMode(mode);
      
      res.status(200).json({ data: updatedDeviceMode, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };
}

export default UsersController;
