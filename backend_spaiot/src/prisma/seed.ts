import { DeviceMode, Gender, PrismaClient, Role, Status } from '@prisma/client';
import { compare, hash } from 'bcrypt';

const prisma = new PrismaClient();
async function main() {
  const password = await hash('password', 10);

  const admin = await prisma.user.upsert({
    where: { username: 'admin' },
    update: {},
    create: {
      username: 'admin',
      password: password,
      role: Role.ADMIN,
      phone: '6285259994137',
      name: 'ADMIN ',
      gender: Gender.MALE,
      address: 'jl.kejawan putih tambak no 16',
    },
  });

  const teacher = await prisma.user.upsert({
    where: { username: 'guru' },
    update: {},
    create: {
      username: 'guru',
      password: password,
      role: Role.TEACHER,
      phone: '6285259994167',
      name: 'demo akun guru',
      gender: Gender.MALE,
      address: 'jl.kejawan putih tambak no 16',
    },
  });

  if ((await prisma.device.count()) === 0) {
    const device = await prisma.device.create({
      data: {
        name: 'Perangkat RFID',
        mode: DeviceMode.READER,
      },
    });

    console.log(device);
  }

  if ((await prisma.classroom.count()) === 0) {
    const classroom = await prisma.classroom.create({
      data: {
        title: 'Kelas 1',
        soundNumber: 100,
        teacherId: teacher.id,
      },
    });

    const student = await prisma.user.upsert({
      where: { username: 'demostudent' },
      update: {},
      create: {
        username: 'demostudent',
        password: password,
        role: Role.STUDENT,
        phone: '6285232267643',
        name: 'User',
        gender: Gender.FEMALE,
        address: 'jl.tunjungan no 18',
        classroomId: classroom.id,
      },
    });

    const parent = await prisma.parent.upsert({
      where: { userId: student.id },
      update: {},
      create: {
        userId: student.id,
        name: 'Parent demostudent',
        phone: '6209875757765',
      },
    });

    const pickupStudent = await prisma.pickup.create({
      data: {
        userId: student.id,
        status: Status.PICKUP,
        date: new Date(Date.now()),
      },
    });

    console.log({ admin, classroom, student, parent, pickupStudent });
  }
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async e => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
