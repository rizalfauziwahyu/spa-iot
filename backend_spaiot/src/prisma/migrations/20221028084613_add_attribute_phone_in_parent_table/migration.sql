/*
  Warnings:

  - Added the required column `phone` to the `parents` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "parents" ADD COLUMN     "phone" TEXT NOT NULL;
