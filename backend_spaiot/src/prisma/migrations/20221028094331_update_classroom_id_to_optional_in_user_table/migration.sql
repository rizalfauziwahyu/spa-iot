-- DropForeignKey
ALTER TABLE "users" DROP CONSTRAINT "users_classroomId_fkey";

-- AlterTable
ALTER TABLE "users" ALTER COLUMN "classroomId" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "users" ADD CONSTRAINT "users_classroomId_fkey" FOREIGN KEY ("classroomId") REFERENCES "Classroom"("id") ON DELETE SET NULL ON UPDATE CASCADE;
