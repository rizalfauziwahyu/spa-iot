-- CreateEnum
CREATE TYPE "DeviceMode" AS ENUM ('WRITER', 'READER');

-- CreateTable
CREATE TABLE "Device" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "mode" "DeviceMode" NOT NULL DEFAULT 'READER',

    CONSTRAINT "Device_pkey" PRIMARY KEY ("id")
);
