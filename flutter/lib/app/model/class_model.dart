import 'package:intl/intl.dart';

class Classroom {
  List<Data>? data;

  Classroom({this.data});

  Classroom.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int? id;
  String? title;
  List<User>? user;

  Data({this.id, this.title});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    if (json['User'] != null) {
      user = <User>[];
      json['User'].forEach((v) {
        user!.add(User.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    return data;
  }
}

class User {
  int? id;
  String? name;
  String? username;
  String? photo;
  String? address;
  String? gender;
  String? phone;
  String? role;
  String? rfid;
  int? classroomId;
  List<Pickup>? pickup;
  ClassroomStudent? classroom;

  User(
      {this.id,
      this.name,
      this.username,
      this.photo,
      this.address,
      this.gender,
      this.phone,
      this.role,
      this.rfid,
      this.classroomId,
      this.classroom});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    photo = json['photo'];
    address = json['address'];
    gender = json['gender'];
    phone = json['phone'];
    role = json['role'];
    rfid = json['rfid'];
    classroomId = json['classroomId'];
    if (json['Pickup'] != null) {
      pickup = <Pickup>[];
      json['Pickup'].forEach((v) {
        pickup!.add(Pickup.fromJson(v));
      });
    }
    if (json['classroom'] != null) {
      classroom = ClassroomStudent.fromJson(json['classroom']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['username'] = username;
    data['name'] = name;
    data['photo'] = photo;
    data['address'] = address;
    data['gender'] = gender;
    data['phone'] = phone;
    data['role'] = role;
    data['rfid'] = rfid;
    data['classroomId'] = classroomId;
    return data;
  }
}

class Pickup {
  String? status;
  String? date;

  Pickup({this.status, this.date});

  Pickup.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    String dateTime = json['date'];
    DateTime parsedDateFormat = DateFormat("yyyy-MM-ddTHH:mm:ssZ")
        .parseUTC(dateTime)
        .toLocal(); // parse String datetime to DateTime and get local date time
    String formatedDateTime =
        DateFormat.yMd().add_jm().format(parsedDateFormat).toString();
    date = formatedDateTime;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['date'] = date;
    return data;
  }
}

class ClassroomStudent {
  int? id;
  String? title;

  ClassroomStudent({this.id, this.title});

  ClassroomStudent.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    return data;
  }
}
