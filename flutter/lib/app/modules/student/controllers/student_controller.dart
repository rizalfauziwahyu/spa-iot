import 'package:app_penjemputan_anak/app/model/class_model.dart';
import 'package:app_penjemputan_anak/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../../../global/globals.dart' as globals;
import 'dart:convert';

class StudentController extends GetxController {
  //TODO: Implement StudentController
  User? student;
  RxBool isLoading = true.obs;

  @override
  void onInit() {
    super.onInit();
    getApi();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  getApi() async {
    try {
      String id = Get.parameters['id']!;
      http.Response response = await http.get(
          Uri.parse("${globals.url}api/v1/users/$id"),
          headers: {"Authorization": 'Bearer ${globals.token}'});
      if (response.statusCode == 200) {
        Map<String, dynamic> result = jsonDecode(response.body);
        student = User.fromJson(result['data']);
        isLoading(false);
      }
    } catch (e) {
      print(e);
    }
  }

  logout() async {
    try {
      http.Response response = await http.post(
          Uri.parse('${globals.url}api/v1/logout'),
          headers: {"Authorization": 'Bearer ${globals.token}'});
      if (response.statusCode == 200) {
        Get.offAllNamed(Routes.AUTH);
      } else {
        Get.offAllNamed(Routes.AUTH);
      }
    } catch (err) {
      Get.offAllNamed(Routes.AUTH);
    }
  }
}
