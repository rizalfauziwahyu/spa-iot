import 'package:app_penjemputan_anak/app/model/class_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../routes/app_pages.dart';
import '../controllers/student_controller.dart';

class StudentView extends GetView<StudentController> {
  final Widget _verticalDivider =
      const VerticalDivider(color: Color.fromRGBO(0, 0, 0, 0.2));

  const StudentView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Siswa'),
            MaterialButton(
              onPressed: () {
                controller.logout();
              },
              color: Colors.white,
              child: const Text(
                'Logout',
                style: TextStyle(
                    color: Color.fromRGBO(30, 41, 60, 1), fontSize: 14),
              ),
            ),
          ],
        ),
        backgroundColor: const Color.fromRGBO(30, 41, 60, 1),
      ),
      body: Column(
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 36, bottom: 24),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Obx(() => Text(
                              controller.isLoading.value
                                  ? 'Loading'
                                  : controller.student!.name!.toString(),
                              style: const TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.w500),
                            )),
                        Obx(() => Text(
                              controller.isLoading.value
                                  ? 'Loading'
                                  : controller.student!.classroom!.title!,
                              style: const TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            )),
                      ],
                    ),
                  ),
                  const SizedBox(height: 40),
                  const Text(
                    'Riwayat Penjemputan',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: ListView(
              physics: const ClampingScrollPhysics(),
              scrollDirection: Axis.vertical,
              children: <Widget>[
                Obx(
                  () => Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                    child: controller.isLoading.value
                        ? const Center(
                            child: Text(
                              'Loading',
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.w500),
                            ),
                          )
                        : DataTable(
                            columnSpacing:
                                (MediaQuery.of(context).size.width / 10) * 0.1,
                            dataRowHeight: 60,
                            border: const TableBorder(
                              top: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                              bottom: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                              left: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                              right: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                            ),
                            columns: <DataColumn>[
                              const DataColumn(
                                label: Text(
                                  'Hari/Tanggal',
                                  style: TextStyle(fontStyle: FontStyle.italic),
                                ),
                              ),
                              DataColumn(label: _verticalDivider),
                              const DataColumn(
                                label: Text(
                                  'Status',
                                  style: TextStyle(fontStyle: FontStyle.italic),
                                ),
                              ),
                            ],
                            rows: List.generate(
                                controller.student!.pickup!.length, (index) {
                              Pickup pickup =
                                  controller.student!.pickup![index];
                              return DataRow(
                                cells: <DataCell>[
                                  DataCell(Text(pickup.date!)),
                                  DataCell(_verticalDivider),
                                  DataCell(Text(pickup.status! == 'PICKUP'
                                      ? 'Sudah Dijemput'
                                      : 'Belum Dijemput')),
                                ],
                              );
                            }),
                          ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
