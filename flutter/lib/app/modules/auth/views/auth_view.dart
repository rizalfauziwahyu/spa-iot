import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/auth_controller.dart';
import '../../../global/globals.dart' as globals;

class AuthView extends GetView<AuthController> {
  const AuthView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('Authentication'),
        backgroundColor: const Color.fromRGBO(30, 41, 60, 1),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Center(
              child: Text(
                'Login',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w400),
              ),
            ),
            const SizedBox(height: 20),
            TextFormField(
              initialValue: controller.usernameData.value,
              onChanged: (value) => controller.onchangeUsername(value),
              decoration: InputDecoration(
                hintText: 'Username',
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Obx(() => TextFormField(
                  initialValue: controller.passwordData.value,
                  obscureText: controller.isPasswordhidden.value,
                  onChanged: (value) => controller.onchangePassword(value),
                  decoration: InputDecoration(
                    hintText: 'Password',
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color.fromRGBO(30, 41, 60, 1)),
                      borderRadius: BorderRadius.circular(3),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color.fromRGBO(30, 41, 60, 1)),
                      borderRadius: BorderRadius.circular(3),
                    ),
                    suffixIcon: IconButton(
                      onPressed: () {
                        controller.changePasswordVisibility();
                      },
                      icon: const Icon(
                        Icons.remove_red_eye,
                        color: Color.fromRGBO(30, 41, 60, 1),
                      ),
                    ),
                  ),
                )),
            const SizedBox(height: 10),
            Obx(() => Center(
                  child: Text(
                    controller.errorData.value,
                    style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(30, 41, 60, 1)),
                  ),
                )),
            const SizedBox(height: 10),
            MaterialButton(
              onPressed: () {
                controller.login();
              },
              color: const Color.fromRGBO(30, 41, 60, 1),
              child: const Text(
                'Submit',
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
