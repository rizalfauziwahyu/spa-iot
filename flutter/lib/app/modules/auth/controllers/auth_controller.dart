import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../../../routes/app_pages.dart';
import '../../../global/globals.dart' as globals;

class AuthController extends GetxController {
  //TODO: Implement AuthController

  RxString usernameData = 'admin'.obs;
  RxString passwordData = 'password'.obs;
  RxString errorData = ''.obs;
  RxBool isPasswordhidden = true.obs;
  RxBool isLoading = false.obs;

  void changePasswordVisibility() =>
      isPasswordhidden.value = !isPasswordhidden.value;
  void onchangeUsername(String username) => usernameData.value = username;
  void onchangePassword(String password) => passwordData.value = password;

  login() async {
    try {
      errorData.value = '';
      isLoading(true);
      Map<String, dynamic> body = {
        "username": usernameData.value,
        "password": passwordData.value
      };
      http.Response response =
          await http.post(Uri.parse('${globals.url}api/v1/login'), body: body);
      if (response.statusCode == 200) {
        Map<String, dynamic> result = jsonDecode(response.body);
        String? authToken = response.headers['set-cookie']?.split(';')[0];
        globals.token = authToken?.split('=')[1];
        String role = result['data']['role'];
        if (role != 'STUDENT') {
          Get.offNamed(Routes.HOME);
        } else {
          int id = result['data']['id'];
          Get.offNamed(Routes.STUDENT,
              parameters: {"id": id.toString(), "kelas": "Kelas 1"});
        }
        isLoading(false);
      } else {
        Map<String, dynamic> result = jsonDecode(response.body);
        errorData.value = result['message'];
      }
    } catch (err) {
      errorData.value = err.toString();
    }
  }
}
