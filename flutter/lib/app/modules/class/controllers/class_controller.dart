import 'dart:convert';
import 'package:app_penjemputan_anak/app/model/class_model.dart';
import 'package:app_penjemputan_anak/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../../../global/globals.dart' as globals;

class ClassController extends GetxController {
  RxBool isLoading = true.obs;
  Data? classroom;

  @override
  void onInit() {
    super.onInit();
    getApi();
  }

  @override
  Future<void> onReady() async {
    super.onReady();
  }

  @override
  void onClose() {}
  getApi() async {
    try {
      int id = Get.arguments;
      http.Response response = await http.get(
          Uri.parse("${globals.url}api/v1/classrooms/$id"),
          headers: {"Authorization": 'Bearer ${globals.token}'});

      if (response.statusCode == 200) {
        Map<String, dynamic> result = jsonDecode(response.body);
        classroom = Data.fromJson(result['data']);
        isLoading(false);
      }
    } catch (e) {
      print(e);
    }
  }

  logout() async {
    try {
      http.Response response = await http.post(
          Uri.parse('${globals.url}api/v1/logout'),
          headers: {"Authorization": 'Bearer ${globals.token}'});
      if (response.statusCode == 200) {
        Get.offAllNamed(Routes.AUTH);
      } else {
        Get.offAllNamed(Routes.AUTH);
      }
    } catch (err) {
      Get.offAllNamed(Routes.AUTH);
    }
  }
}
