import 'package:app_penjemputan_anak/app/model/class_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../routes/app_pages.dart';
import '../controllers/class_controller.dart';

class ClassView extends GetView<ClassController> {
  final Widget _verticalDivider =
      const VerticalDivider(color: Color.fromRGBO(0, 0, 0, 0.2));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Kelas'),
            MaterialButton(
              onPressed: () {
                controller.logout();
              },
              color: Colors.white,
              child: const Text(
                'Logout',
                style: TextStyle(
                    color: Color.fromRGBO(30, 41, 60, 1), fontSize: 14),
              ),
            ),
          ],
        ),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(30, 41, 60, 1),
      ),
      body: Column(
        children: [
          Center(
            child: Padding(
              padding: EdgeInsets.only(top: 36, bottom: 24),
              child: Column(
                children: <Widget>[
                  Obx(() => Text(
                        controller.isLoading.value
                            ? 'Loading'
                            : controller.classroom!.title!,
                        style: const TextStyle(
                            fontSize: 24, fontWeight: FontWeight.w500),
                      )),
                  const SizedBox(height: 10),
                  const Text(
                    'Penjemputan hari ini',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: ListView(
              physics: const ClampingScrollPhysics(),
              scrollDirection: Axis.vertical,
              children: <Widget>[
                Obx(
                  () => Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                    child: controller.isLoading.value
                        ? const Center(
                            child: Text(
                              'Loading',
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.w500),
                            ),
                          )
                        : DataTable(
                            columnSpacing:
                                (MediaQuery.of(context).size.width / 10) * 0.1,
                            dataRowHeight: 60,
                            border: const TableBorder(
                              top: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                              bottom: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                              left: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                              right: BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.1)),
                            ),
                            columns: <DataColumn>[
                              const DataColumn(
                                label: Text(
                                  'Nama',
                                  style: TextStyle(fontStyle: FontStyle.italic),
                                ),
                              ),
                              DataColumn(label: _verticalDivider),
                              const DataColumn(
                                label: Text(
                                  'Nomor',
                                  style: TextStyle(fontStyle: FontStyle.italic),
                                ),
                              ),
                              DataColumn(label: _verticalDivider),
                              const DataColumn(
                                label: Text(
                                  'Aksi',
                                  style: TextStyle(fontStyle: FontStyle.italic),
                                ),
                              ),
                            ],
                            rows: List.generate(
                                controller.classroom!.user!.length, (index) {
                              User student = controller.classroom!.user![index];
                              return DataRow(
                                cells: <DataCell>[
                                  DataCell(Text(student.name!)),
                                  DataCell(_verticalDivider),
                                  DataCell(Text(student.phone!)),
                                  DataCell(_verticalDivider),
                                  DataCell(
                                    MaterialButton(
                                      onPressed: () {
                                        Get.toNamed(Routes.STUDENT,
                                            parameters: {
                                              "id": student.id!.toString(),
                                              "kelas":
                                                  controller.classroom!.title!
                                            });
                                      },
                                      color: Color.fromRGBO(30, 41, 60, 1),
                                      child: const Text(
                                        'Detail',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            }),
                          ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
