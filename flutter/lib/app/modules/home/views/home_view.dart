import 'package:flutter/material.dart';

import 'package:get/get.dart';
import '../controllers/home_controller.dart';
import '../../../routes/app_pages.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Home'),
            MaterialButton(
              onPressed: () {
                controller.logout();
              },
              color: Colors.white,
              child: const Text(
                'Logout',
                style: TextStyle(
                    color: Color.fromRGBO(30, 41, 60, 1), fontSize: 14),
              ),
            ),
          ],
        ),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(30, 41, 60, 1),
      ),
      body: Column(
        children: <Widget>[
          const Center(
            child: Padding(
              padding: EdgeInsets.only(top: 36, bottom: 36),
              child: Text(
                'Daftar Kelas',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
              ),
            ),
          ),
          Obx(
            () => Expanded(
              child: controller.isLoading.value
                  ? const Text(
                      'Loading',
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
                    )
                  : ListView.builder(
                      physics: const ClampingScrollPhysics(),
                      itemCount: controller.classroom!.data!.length,
                      itemBuilder: (context, index) => Column(
                        children: <Widget>[
                          ElevatedButton(
                            onPressed: () {
                              Get.toNamed(Routes.CLASS,
                                  arguments:
                                      controller.classroom!.data![index].id);
                            },
                            style: ElevatedButton.styleFrom(
                              padding: const EdgeInsets.only(
                                  left: 64, right: 64, top: 28, bottom: 28),
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                              ),
                              backgroundColor: Color.fromRGBO(30, 41, 60, 1),
                              minimumSize: const Size(10, 10),
                            ),
                            child: Text(
                              controller.classroom!.data![index].title!,
                              style: const TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w400),
                            ),
                          ),
                          const SizedBox(height: 24),
                        ],
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
